<?php
  session_start();

  unset($_SESSION['user_session']);
  unset($_SESSION['isAdmin']);

  if(session_destroy()) {
    header("Location: ../admin/login.php");
  }
?>
