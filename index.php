<?php
	
	/** Database connection and Objetcs **/
  include_once 'config/database.php';
  include_once 'objetcs/sliderimage.php';
  include_once 'objetcs/aboutus.php';
  include_once 'objetcs/worldcertification.php';
  include_once 'objetcs/ourrecipe.php';
  include_once 'objetcs/internationalalliance.php';
  include_once 'objetcs/customerproject.php';
  include_once 'objetcs/personaltestimony.php';

  $database = new Database();
  $db = $database->getConnection();

  $sliderimage = new SliderImage($db);
  $aboutus = new AboutUs($db);
  $worldcertification = new WorldCertification($db);
  $ourrecipe = new OurRecipe($db);
  $internationalalliance = new InternationalAlliance($db);
  $customerproject = new CustomerProject($db);
  $personaltestimony = new PersonalTestimony($db);

  /** Imagenes en el Slider  Section **/

  $targetSliderImage = $sliderimage->readSliderImage();
  $num = $targetSliderImage->rowCount();

  $itemSliderImage = $sliderimage->readSliderImage();
  $num2 = $itemSliderImage->rowCount();

  /** Nosotros Section **/
  $itemsAboutUs = $aboutus->readAboutUs();
  $num3 = $itemsAboutUs->rowCount();

  /** Certificación Mundial Section **/
  $itemsWorldCertification = $worldcertification->readWorldCertification();
  $num4 = $itemsWorldCertification->rowCount();

  /** Nuestra Receta Section**/
  $itemsOurRecipe = $ourrecipe->readOurRecipe();
  $num5 = $itemsOurRecipe->rowCount();

  /** Alianza Internaction Section**/
  $itmesInternationalAlliance = $internationalalliance->readInternationalAlliance();
  $num6 = $itmesInternationalAlliance->rowCount();

  /** Proyectos Section **/
  $itemsCustomerProject = $customerproject->readCustomerProject();
  $num7 = $itemsCustomerProject->rowCount();

  /** Testimonios Section **/
  $itemsPersonalTestimony = $personaltestimony->readPersonalTestimony();
  $num8 = $itemsPersonalTestimony->rowCount();

?>
<!DOCTYPE html>
<html>
<head>
	<title>Food Gurus</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="keywords" content="Restaurante, Interiores, Remodelación, Cocina, Chef">
  <meta name="description" content="Food Gurus">
  <meta name="author" content="Code Castle El Salvador - Web Developer: Carlos Faustino">
  <link rel="shortcut icon" href="images/fg.png" type="image/x-icon">
	<!-- Stylesheets Section - Begin -->
	<?php include("include/stylesheet.php"); ?>
	<!-- Stylesheets Section --->

	<!-- Captcha -->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
<!-- Menú Navbar - Begin -->
	<?php include ("include/header.php"); ?>
<!-- Menú Navbar - End -->

<!-- Main Slider - Begin -->
	<main role="main">
    <div id="myCarousel" class="carousel slide main-carousel" data-ride="carousel">
      <ol class="carousel-indicators">
        <?php
              if ($num > 0) {
                  $j = 0;
                  while ($row = $targetSliderImage->fetch(PDO::FETCH_ASSOC)) {
                      extract($row);
                      $active1 = ($j==0) ? "active" : "";
                      echo "<li data-target='#myCarousel' data-slide-to='".$j."' class='".$active1."'></li>";
                      $j++;
                  }
              } else {

              }
          ?>
      </ol>
      <div class="carousel-inner main-carousel-inner">
      	<?php
            if ($num2 > 0) {
                $i = 0;
                while ($row = $itemSliderImage->fetch(PDO::FETCH_ASSOC)) {
                    extract($row);
                    $active = ($i==0) ? "active" : "";
                    echo "<div class='carousel-item ".$active."'>";
                        echo "<img class='third-slide' src='{$si_image}' alt='{$si_name}'>";
                        echo "<div class='container'>";
							            echo "<div class='carousel-caption text-center main-carousel-caption d-none d-sm-block'>";
							              echo "{$si_description}";
							            echo "</div>";
							          echo "</div>";
                    echo "</div>";
                    $i++;
                }
            }
          ?>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </main>
<!-- Main Slider - End -->

<!-- Nosotros - Begin -->
	<div class="container-fluid margin-padding-none nosotros-section" id="nosotros" style="position: relative;">
		<div class="row margin-padding-none">
			<div class="col-12 col-md-6" style="background-color: #fff;">
				<h1 class="text-center">Nosotros</h1>
				<div class="d-block d-sm-none col-8 offset-2">
					<img src="images/items/foto4.jpg" class="img-fluid rounded-circle" style="margin: auto; width: 100%; height: 100%; margin-bottom: 2rem; border: 4px solid #000;">
				</div>
				<div class="col-10 offset-1 col-md-6 offset-md-3">
					<img src="images/items/foto4.jpg" class="img-fluid rounded-circle d-none d-sm-block" alt="Juan Matamoros" style="width: 250px; height: 250px; border: 2px solid #000; margin: auto;">
					<br>
				</div>
				<div class="col-12 col-md-12">
					<?php
	            if ($num3 > 0) {
	              while ($row = $itemsAboutUs->fetch(PDO::FETCH_ASSOC)) {
	              extract($row);
	                echo $au_section;
	              } 
	            }
						?>
				</div>
			</div>
			<div class="col-md-6" style="margin: 0px; padding: 0px;position: relative; background-image: url('images/items/foodgurugif.gif'); background-repeat: no-repeat; background-size: 175% 100%;">
			</div>
		</div>
	</div>
<!-- Nosotros - End -->

<!-- Certificación Internacional - Begin -->
	<div class="container-fluid margin-padding-none certificacion-section" id="certificacionInternacional">
		<div class="row margin-padding-none">
			<div class="col-12 col-md-12">
				<h1 class="text-center">Certificación Mundial</h1>
			</div>
		</div>
		<div class="row margin-padding-none">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-10">
						<?php
	            if ($num4 > 0) {
	              while ($row = $itemsWorldCertification->fetch(PDO::FETCH_ASSOC)) {
	              extract($row);
	                echo $wc_section;
	              } 
	            }
						?>
					</div>
					<div class="col-8 offset-2 col-md-2 offset-md-0" style="margin: auto;">
						<br>
						<a href="https://www.fcsi.org/" target="_blank" title="Haz click para visitar www.fcsi.org">
							<img src="images/items/fcsi.png" class="img-fluid" alt="FSCI">
						</a>
						<br><br>
					</div>
				</div>
			</div>
		</div>
		<div class="row margin-padding-none">
			<div class="container">
				<div class="col-12 col-md-12">
					<br><br>
					<h2 class="text-center">¿Que hace un consultor de FCSI?</h2>
					<br>
          <iframe width="100%" height="500px" src="https://www.youtube.com/embed/C9AsNlv_U9Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				        <!--<video src="multimedia/foodgurus.mp4" width="100%" height="auto" controls="">
					  Tu navegador no implementa el elemento <code>video</code>.
					</video>-->
					<br>
				</div>
			</div>
		</div>
	</div>
<!-- Certificación Internacional - End -->

<!-- Nuestra Receta - Begin -->
	<div class="container-fluid margin-padding-none nuestra-receta-section" id="nuestraReceta">
		<div class="row margin-padding-none">
			<div class="col-12 col-md-12">
				<h1 class="text-center">Nuestra Receta<br><small>Servicios</small></h1>
				<h3 class="text-center"><small>Estos son los servicios por áreas que ofrecemos en Food Gurus</small></h3>
			</div>
		</div>
		<div class="row margin-padding-none">
			<div class="col-12 col-md-12">
				<!-- Slider Nuestra Receta - Begin -->
				<div class="nuestra-receta-slider">
		      <div class="row margin-padding-none eliminarTexto" id="eliminarTexto" onload="quitarTexto()">
		      	<div class="col-12">
		      		<div id="thumbnail-slider">
			          <div class="inner">
		              <ul style="position: relative;">
		              	<?php
					            if ($num5 > 0) {
					              while ($row = $itemsOurRecipe->fetch(PDO::FETCH_ASSOC)) {
					              extract($row);
					                echo "<li>";
					                  echo "<img src='{$or_image}'' class='thumb'>";
					                  echo "<a href='#{$ID}'' class='fancybox hvr-pulse-grow' style='width: 100%; height: 100%;'>";
															echo "<div class='col-md-12 overlay-img'>";
					                  	echo "</div>";
														echo "</a>";
														echo "<div id='{$ID}' class='container nuestra-receta-fancybox' style='display: none; margin: auto;'>";
															echo "<div class='row margin-padding-none'>";
																echo "<div class='col-12 col-md-10 offset-md-1'>";
																	echo "<h3 class='text-center'>".$or_name."</h3>";
																	echo "<br>";
																echo "</div>";
															echo "</div>";
															echo "<div class='row margin-padding-none'>";
																echo "<div class='col-12 col-md-10 offset-md-1'>";
																		echo $or_description;
																echo "</div>";
															echo "</div>";
														echo "</div>";
					                echo "</li>";
					              } 
					            }
						        ?>
		              </ul>
			          </div>
				      </div>
		      	</div>
		      </div>
		  	</div>
		  	<!-- Slider Nuestra Receta - End -->
			</div>
		</div>
		<div class="col-md-12 gradient-effect" style="padding: 0px;">
			<p></p>
		</div>
	</div>
<!-- Nuestra Receta - End -->

<!-- Alianza - Begin -->
	<div class="container alianza-section" id="alianzaInternacional">
		<div class="row margin-padding-none">
			<div class="col-md-12">
				<h1 class="text-center" style="z-index: 1000">Alianza Internacional</h1>
			</div>
		</div>
		<div class="row margin-padding-none">
			<div class="col-10 offset-1 col-md-5">
				<img src="images/items/logo-gastrotec.png" class="img-fluid" alt="Gastrotec" style="margin: auto; width: 100%;"><br><br>
			</div>
			<br>
			<div class="col-12 col-md-6">
				<?php
          if ($num6 > 0) {
            while ($row = $itmesInternationalAlliance->fetch(PDO::FETCH_ASSOC)) {
            extract($row);
              echo $ia_section;
            } 
          }
        ?>
			</div>
		</div>
	</div>
<!-- Alianza - End -->

<!-- Nuestros Clientes - Begin -->
	<div class="container-fluid margin-padding-none clientes-section" id="proyectos">
		<div class="row margin-padding-none">
			<div class="col-md-12">
				<h1 class="text-center" style="z-index: 1000">Ellos confiaron en nosotros</h1>
			</div>
		</div>
		<div class="row margin-padding-none">
			<div class="container">
				<div class="container-fluid nuestros-clientes">
			    <section class="customer-logos slider">
			    	<?php
	            if ($num7 > 0) {
	              while ($row = $itemsCustomerProject->fetch(PDO::FETCH_ASSOC)) {
	              extract($row);
		              echo "<div class='col-xs-6 col-sm-4 col-md-3 hvr-grow grises'>";
		              	echo "<img src='{$cp_image}' alt='{$cp_name}'>";
		              echo "</div>";
	              } 
	            }
		        ?>
			    </section>
  			</div>
			</div>
		</div>
		<div class="col-md-12 overlay-img" style="padding: 0px;">
			<p></p>
		</div>
	</div>
<!-- Nuestros Clientes - End -->

<!-- Redes Sociales - Begin -->
	<div class="container-fluid margin-padding-none redes-sociales-section">
		<div class="row margin-padding-none">
			<div class="col-12 col-md-8 margin-padding-none">
				<h1 class="text-center">SÍGUENOS EN LAS REDES</h1>
			</div>
			<div class="col-12 col-md-4 text-center margin-padding-none">
				<a href="https://www.facebook.com/foodguruscentroamerica" target="_blank" title="Visitar Fan Page de Facebook">
					<img src="images/items/fb.png" class="img-fluid hvr-pulse" alt="Facebook">
				</a>
				<a href="http://www.instagram.com" target="_blank" title="Visitar Food Gurus en Instagram">
					<img src="images/items/in.png" class="img-fluid hvr-pulse" alt="Instagram">
				</a>
				<a href="https://www.youtube.com/watch?v=C9AsNlv_U9Q" target="_blank" title="Visitar Canal de Youtube">
					<img src="images/items/yt.png" class="img-fluid hvr-pulse" alt="Youtube">
				</a>
			</div>
		</div>
	</div>
<!-- Redes Sociales - End -->

<!-- Testimonios - Begin -->
	<div class="container-fluid testimonio-section margin-padding-none" id="testimoniales">
		<div class="row margin-padding-none">
			<div class="col-12 col-md-12">
				<h1 class="text-center">Testimoniales</h1>
			</div>
		</div>
		<div class="row margin-padding-none bg-testimonio">
			<?php
        if ($num8 > 0) {
          while ($row = $itemsPersonalTestimony->fetch(PDO::FETCH_ASSOC)) {
          extract($row);
          echo "<div class='col-12 col-sm-6 col-md-4 img-wrap hvr-sink'>";
						echo "<div class='row'>";
							echo "<div class='col-6 offset-3 col-sm-8 offset-sm-2 col-md-6 offset-md-3'>";
								echo "<img src='{$pt_image}' class='img-fluid rounded-circle' alt='{$pt_name}'>";
							echo "</div>";
							echo "<div class='col-12'>";
								echo "<h5 class='text-center'>".$pt_name."</h5>";
								echo "<h6 class='text-center'>".$pt_job."</h6>";
							echo "</div>";
							echo "<div class='img-description-layer'>";
								echo $pt_description;
							echo "</div>";
						echo "</div>";
					echo "</div>";
          } 
        }
      ?>
			<div class="overlay-dark"></div>
		</div>
		
	</div>
<!-- Testimonios - End -->

<!-- Contacto - Begin -->
	<div class="container-fluid contacto-section margin-padding-none" id="contactanos">
		<div class="row margin-padding-none">
			<div class="col-12 col-md-12">
				<h1 class="text-center">Contáctanos</h1>
			</div>
		</div>
		<div class="row margin-padding-none">
			<div class="col-12 col-md-4 offset-md-1">
				<form class="col-12 contactForm" id="contactForm" name="contactForm" method="post">
					<div class="form-group">
						<input type="text" name="nombre" id="nombre" placeholder="Nombre Completo" class="col-12 col-md-11" required="">
					</div>
					<div class="form-group">
						<input type="email" name="correo" id="correo" placeholder="Correo Eléctronico" class="col-12 col-md-11" required="">
					</div>
					<div class="form-group">
						<input type="phone" name="telefono" id="telefono" placeholder="Teléfono" class="col-12 col-md-11" required="">
					</div>
					<div class="form-group">
						<textarea name="mensaje" id="mensaje" placeholder="Mensaje" rows="5" class="col-12 col-md-11"></textarea>
					</div>
					<div class="form-group">
						<div class="g-recaptcha col-12" data-sitekey="6LdDKWIUAAAAAGXrP_ma8J4WSsHdIilOoZ6ZiaPI"></div>
					</div>
					<div class="form-group">
						<input type="submit" name="btnContacto" id="btnContacto" class="col-12 col-md-3 offset-md-4 btn btn-dark btnContacto" value="Enviar">
					</div>
				</form>
			</div>
			<div class="col-12 col-md-6">
				<a href="">
					<img src="images/items/mapa.png" class="img-fluid" alt="Mapa de Ubicación Food Gurus" style="width: 100%; height: 400px; margin: auto;">
				</a>
			</div>
		</div>
	</div>
<!-- Contacto - End -->

<!-- Mensajes de Alerta de Envio de Mensaje - Contáctenos -->
<div class="msg1" style='display:none'><p>Correo enviado éxitosamente.</p></div>
<div class="msg2" style='display:none'><p>No deje campos vacios.</p></div>
<div class="msg3" style='display:none'><p>Verifique que los campos esten correctos.</p></div>
<div class="msg4" style='display:none'><p>Por favor Chequee el ReCaptcha</p></div>	

<!-- Footer - Begin -->
	<?php include("include/footer.php"); ?>
<!-- Footer - End -->
<!-- Scripts Section - Begin -->
	<?php include ("include/script.php"); ?>
<!-- Scripts Section - End -->
</body>
</html>