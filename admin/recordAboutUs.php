<?php
    session_start();

    if(!isset($_SESSION['user_session']))
    {
     header("Location: login.php");
    } else {
        $user_id = $_SESSION['user_id'];
    }

    include_once "../config/database.php";
    include_once "../objetcs/user.php";
    include_once "../objetcs/aboutus.php";

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);
    $aboutus = new AboutUs($db);

    $user->user_id = $user_id;
    $user->getUser();

    $value = "nAboutUs";
    $input = "";

    if (isset($_GET['ID'])) {
        $aboutUsID = isset($_GET['ID']) ? $_GET['ID'] : die('ERROR: Sección ID not found.');
        $input = "<input type='hidden' name='ID' value='{$aboutUsID}' />";
        $option         = isset($_GET['opt']) ? $_GET['opt'] : die('ERROR: Option not found.');
        $value          = $option == "mAboutUs" ? "mAboutUs" : "nAboutUs";
        $aboutus->ID = $aboutUsID;
        $aboutus->readOne();
    }

 ?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrador Food Gurus</title>
    <meta name="description" content="Food Gurus Agencia Gastronomica Administrador de Sitio web">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="images/fg.png">

    <!-- Stylesheet -->
    <?php include("assets/include/stylesheet.php"); ?>

    <!-- CKeditor -->
    <script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>

</head>
<body>
  
  <!-- Left Panel -->
  <?php include ("assets/include/navbar-left.php"); ?>
  <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
      <!-- Header -->
      <?php include("assets/include/header.php"); ?>
      <!-- Header -->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Dashboard</a></li>
                            <li><a href="recordAboutUs.php">Sección Nosotros</a></li>
                            <li class="active">Nosotros</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">


                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <strong>Nosotros</strong> Sección
                      </div>
                      <div class="card-body card-block">
                        <form action="#" id="aboutUsForm" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="opt" value="<?php echo $value; ?>">
                        <?php echo $input; ?>
                          <div class="row form-group">
                            <div class="col-12 col-md-12">
                              <textarea name="au_section" id="au_section" rows="9" placeholder="Content..." class="form-control ckeditor">
                                <?php if(isset($aboutus->au_section)) echo $aboutus->au_section; ?>
                              </textarea>
                              <small class="form-text text-muted">El contenido de este campo se mostrará en el lado izquierdo de la sección de Nosotros.</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-12">
                              <div class="msg">
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-8">
                              <a href="index.php" class="btn btn-secondary">Cancelar</a>
                            </div>
                            <div class="col col-md-4">
                              <button type="reset" class="btn btn-secondary">
                                <i class="fa fa-ban"></i> Limpiar
                              </button>
                              <button type="submit" class="btn btn-info">
                                <i class="fa fa-save"></i>
                                 <?php echo $value == "nAboutUs" ? "Guardar Sección" : "Actualizar Sección"; ?>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="card-footer"> 
                      </div>
                    </div>
                  </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
    
    <script src="assets/js/app.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/parsley.min.js" type="text/javascript"></script>
    <script src="assets/js/select2.min.js" type="text/javascript"></script>
    <script src="assets/js/app-form-elements.js" type="text/javascript"></script>

    <!-- JQuery Add & Update data -->
    <script type="text/javascript">

      $(document).ready(function(){
          //initialize the javascript
          App.init();
          $('form').parsley();
          App.formElements();

          $("#aboutUsForm").on('submit',(function(e) {
              e.preventDefault();

              for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
              }
              
              $.ajax({
                  type : 'POST',
                  url  : '../objetcs/action.php',
                  data : new FormData(this),
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function(){
                      if ($("#au_section").val() == "") {
                          message = "Por favor escriba el contenido de la sección.";
                          error = true;
                      } else {
                          error = false;
                      }

                      if (error == true){
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Warning!</strong> '+message+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          return false;
                      }
                  },
                  success :  function(response)   {
                      var parsed = JSON.parse(response);
                      if(parsed.title=="Success"){
                          //$("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Signing In ...');
                          var msg = '<div class="alert alert-success alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-check"></span>'+
                                    '<strong>Warning!</strong> '+parsed.text+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          // fade out effect first
                          $(msg).fadeOut('slow', function(){
                              setTimeout(' window.location.href = "recordAboutUs.php?ID=1&opt=mAboutUs"; ',1500);
                          });
                      }else{
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Advertencia!</strong> '+parsed.text+'</div>';
                          $(".msg").append(msg).fadeIn("slow");
                      }
                  }
              });
              return false;
          }));
      });

      [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
        new SelectFx(el);
      } );

        $('.selectpicker').selectpicker;


        $('#menuToggle').on('click', function(event) {
          $('body').toggleClass('open');
        });

        $('.search-trigger').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').addClass('open');
        });

        $('.search-close').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').removeClass('open');
        });

    </script>


</body>
</html>
