<?php
    
    session_start();

    if(!isset($_SESSION['user_session']))
    {
     header("Location: login.php");
    } else {
        $user_id = $_SESSION['user_id'];
    }
    
    include_once "../config/database.php";
    include_once "../objetcs/user.php";
    include_once "../objetcs/personaltestimony.php";

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);
    $personaltestimony = new PersonalTestimony($db);

    $user->user_id = $user_id;
    $user->getUser();

    $verPersonalTestimony = $personaltestimony->readAll();

    header('Content-Type: text/html; charset=UTF-8');
 ?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrador Food Gurus</title>
    <meta name="description" content="Food Gurus Agencia Gastronomica Administrador de Sitio web">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="images/fg.png">

    <!-- Stylesheet -->
    <?php include("assets/include/stylesheet.php"); ?>

</head>
<body>

  <!-- Left Panel -->
  <?php include ("assets/include/navbar-left.php"); ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">

    <!-- Header -->
    <?php include("assets/include/header.php"); ?>
    <!-- Header -->

    <div class="breadcrumbs">
      <div class="col-sm-4">
        <div class="page-header float-left">
          <div class="page-title">
            <h1>Registro de Testimonios</h1>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="page-header float-right">
          <div class="page-title">
            <ol class="breadcrumb text-right">
              <li><a href="index.php">Dashboard</a></li>
              <li><a href="recordPersonalTestimonies.php">Registro de Testimonios</a></li>
              <li class="active">Testimoniales</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="col-md-4">
        <a href="newPersonalTestimony.php" class="btn btn-info"><i class="fa fa-plus-square" aria-hidden='true'></i> Agregar testimonio</a>
      </div>
    </div>
    <div class="content mt-3">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <strong class="card-title">Testimoniales</strong>
              </div>
              <div class="card-body">
                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Imagen</th>
                      <th>Nombre</th>
                      <th>Cargo o Puesto de Trabajo</th>
                      <th>Estado</th>
                      <th>Fecha Creación</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if ($verPersonalTestimony) {
                        $i = 0;
                        while ($row = $verPersonalTestimony->fetch(PDO::FETCH_ASSOC)) {
                          extract($row);
                          if ( $i % 2) {
                            $class = "even";
                          } else {
                            $class = "odd";
                          }
                          echo "<tr'>";
                              echo "<td class='text-center' style='vertical-align:middle;width:20%;'><img src='../{$pt_image}' class='img-responsive' style='width:100%; height: auto; margin: auto;'/></td>";
                              echo "<td class='text-center' style='vertical-align:middle; color: #000; font-weight: bold; width: 25%;'>".($pt_name)."</td>";
                              echo "<td class='text-center' style='vertical-align:middle; color: #000; font-weight: bold; width: 25%;'>".($pt_job)."</td>";
                              if ($pt_status==1) {
                                echo "<td class='text-center' style='vertical-align:middle; color: #fff; background-color:#05A712; font-weight: bold; width:8%;'>Activado</td>";
                              } else {
                                echo "<td class='text-center' style='vertical-align:middle; color: #fff; background-color:#EAA600; font-weight: bold; width:8%;'>Desactivado</td>";
                              }
                              echo "<td class='text-center' style='vertical-align:middle; width:10%;'>{$pt_created_at}</td>";
                              echo "<td class='text-center' style='vertical-align:middle; width:5%;'><a href='newPersonalTestimony.php?ID={$ID}&opt=mPersonalTestimony'><b><i class='fa fa-pencil-square-o' aria-hidden='true'></i></b></a></td>";
                              echo "<td class='center' style='vertical-align:middle; width:5%; margin: auto; text-align: center;'>
                                  <a class='delete-record' data-toggle='modal' data-target='#{$ID}'>
                                    <i class='fa fa-times' aria-hidden='true'></i>
                                  </a>
                                  <!-- Modal -->
                                    <div class='modal fade' id='{$ID}' role='dialog'>
                                      <div class='modal-dialog'>
                                      
                                        <!-- Modal content-->
                                        <div class='modal-content'>
                                          <div class='modal-header'>
                                            <h4 class='modal-title'>Eliminar Testimonio</h4>
                                            <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                          </div>
                                          <div class='modal-body'>
                                            <p>¿Esta seguro de eliminar testimonio?</p>
                                          </div>
                                          <div class='modal-footer'>
                                            <a href='../objetcs/action.php?ID={$ID}&opt=ePersonalTestimony' class='btn btn-danger'>Eliminar</a>
                                            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                                          </div>
                                        </div>
                                        
                                      </div>
                                    </div>
                                </td>";
                          echo "</tr>";
                        }
                      } else {
                        echo "<tr>";
                          echo "<td colspan='7'>Registros no encontrados.</td>";
                        echo "</tr>";
                      }
                  ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </div><!-- .content -->

    <?php include("assets/include/footer.php"); ?>

  </div><!-- /#right-panel -->

  <!-- Right Panel -->

    <?php include ("assets/include/script-tables.php"); ?>

    <script type="text/javascript">
      [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
        new SelectFx(el);
      } );

        $('.selectpicker').selectpicker;


        $('#menuToggle').on('click', function(event) {
          $('body').toggleClass('open');
        });

        $('.search-trigger').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').addClass('open');
        });

        $('.search-close').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').removeClass('open');
        });

    </script>

</body>
</html>
