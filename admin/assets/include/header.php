<!-- Header-->
<header id="header" class="header">
  <div class="header-menu">
    <div class="col-sm-7">
        <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-bars"></i></a>
    </div>
    <div class="col-sm-5">
        <div class="user-area dropdown float-right">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo utf8_decode($_SESSION['user_session']); ?> <i class="fa fa-cog"></i>
            </a>

            <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="../index.php" target="_blanck"><i class="fa fa-globe"></i>  Ir al Sitio Web</a>
                    <a class="nav-link" href="../config/logout-admin.php"><i class="fa fa-power-off"></i>  Cerrar Sesión</a>
            </div>
        </div>
    </div>
  </div>
</header>
<!-- Header-->