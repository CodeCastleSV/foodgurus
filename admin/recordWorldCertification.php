<?php
    session_start();

    if(!isset($_SESSION['user_session']))
    {
     header("Location: login.php");
    } else {
        $user_id = $_SESSION['user_id'];
    }

    include_once "../config/database.php";
    include_once "../objetcs/user.php";
    include_once "../objetcs/worldcertification.php";

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);
    $worldcertification = new WorldCertification($db);

    $user->user_id = $user_id;
    $user->getUser();

    $value = "nWorldCertification";
    $input = "";

    if (isset($_GET['ID'])) {
        $worldcertificationID = isset($_GET['ID']) ? $_GET['ID'] : die('ERROR: Sección ID not found.');
        $input = "<input type='hidden' name='ID' value='{$worldcertificationID}' />";
        $option         = isset($_GET['opt']) ? $_GET['opt'] : die('ERROR: Option not found.');
        $value          = $option == "mWorldCertification" ? "mWorldCertification" : "nWorldCertification";
        $worldcertification->ID = $worldcertificationID;
        $worldcertification->readOne();
    }

 ?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrador Food Gurus</title>
    <meta name="description" content="Food Gurus Agencia Gastronomica Administrador de Sitio web">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="images/fg.png">

    <!-- Stylesheet -->
    <?php include("assets/include/stylesheet.php"); ?>

    <!-- CKeditor -->
    <script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>

</head>
<body>
  
  <!-- Left Panel -->
  <?php include ("assets/include/navbar-left.php"); ?>
  <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
      <!-- Header -->
      <?php include("assets/include/header.php"); ?>
      <!-- Header -->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Dashboard</a></li>
                            <li><a href="recordAboutUs.php">Sección Certificación Mundial</a></li>
                            <li class="active">Certificación Mundial</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <strong>Certificación Mundial</strong> Sección
                      </div>
                      <div class="card-body card-block">
                        <form action="#" id="worldCertificationForm" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="opt" value="<?php echo $value; ?>">
                        <?php echo $input; ?>
                          <div class="row form-group">
                            <div class="col-12 col-md-12">
                              <textarea name="wc_section" id="wc_section" rows="9" placeholder="Content..." class="form-control ckeditor">
                                <?php if(isset($worldcertification->wc_section)) echo $worldcertification->wc_section; ?>
                              </textarea>
                              <small class="form-text text-muted">El contenido de este campo se mostrará en la sección de Certificación Mundial.</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-12">
                              <div class="msg">
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-8">
                              <a href="index.php" class="btn btn-secondary">Cancelar</a>
                            </div>
                            <div class="col col-md-4">
                              <button type="reset" class="btn btn-secondary">
                                <i class="fa fa-ban"></i> Limpiar
                              </button>
                              <button type="submit" class="btn btn-info">
                                <i class="fa fa-save"></i>
                                 <?php echo $value == "nWorldCertification" ? "Guardar Sección" : "Actualizar Sección"; ?>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

        <?php include("assets/include/footer.php"); ?>

    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
    
    <script src="assets/js/app.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/parsley.min.js" type="text/javascript"></script>
    <script src="assets/js/select2.min.js" type="text/javascript"></script>
    <script src="assets/js/app-form-elements.js" type="text/javascript"></script>

    <!-- JQuery Add & Update data -->
    <script type="text/javascript">

      $(document).ready(function(){
          //initialize the javascript
          App.init();
          $('form').parsley();
          App.formElements();

          $("#worldCertificationForm").on('submit',(function(e) {
              e.preventDefault();

              for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
              }
              
              $.ajax({
                  type : 'POST',
                  url  : '../objetcs/action.php',
                  data : new FormData(this),
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function(){
                      if ($("#wc_section").val() == "") {
                          message = "Por favor escriba el contenido de la sección.";
                          error = true;
                      } else {
                          error = false;
                      }

                      if (error == true){
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Warning!</strong> '+message+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          return false;
                      }
                  },
                  success :  function(response)   {
                      var parsed = JSON.parse(response);
                      if(parsed.title=="Success"){
                          //$("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Signing In ...');
                          var msg = '<div class="alert alert-success alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-check"></span>'+
                                    '<strong>Warning!</strong> '+parsed.text+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          // fade out effect first
                          $(msg).fadeOut('slow', function(){
                              setTimeout(' window.location.href = "recordWorldCertification.php?ID=1&opt=mWorldCertification"; ',1500);
                          });
                      }else{
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Advertencia!</strong> '+parsed.text+'</div>';
                          $(".msg").append(msg).fadeIn("slow");
                      }
                  }
              });
              return false;
          }));
      });

      [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
        new SelectFx(el);
      } );

        $('.selectpicker').selectpicker;


        $('#menuToggle').on('click', function(event) {
          $('body').toggleClass('open');
        });

        $('.search-trigger').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').addClass('open');
        });

        $('.search-close').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').removeClass('open');
        });

    </script>


</body>
</html>
