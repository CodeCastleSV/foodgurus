<?php
session_start();

$user_id = "";

if(!isset($_SESSION['user_session']))
{
 header("Location: login.php");
} else {
    $user_id = $_SESSION['user_id'];
}

include_once '../config/database.php';
include_once '../objetcs/user.php';

$database = new Database();
$db = $database->getConnection();

$user = new User($db);

$user->user_id = $user_id;
$user->getUser();

?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrador Food Gurus</title>
    <meta name="description" content="Food Gurus Agencia Gastronomica Administrador de Sitio web">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="images/fg.png">

    <!-- Stylesheet -->
    <?php include("assets/include/stylesheet.php"); ?>

</head>
<body>

  <!-- Left Panel -->
  <?php include ("assets/include/navbar-left.php"); ?>
  <!-- Left Panel -->

  <!-- Right Panel -->
  <div id="right-panel" class="right-panel">
    <!-- Header -->
    <?php include("assets/include/header.php"); ?>
    <!-- Header -->
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="col-sm-12 col-lg-4 offset-lg-4 text-center">
            <img src="images/foodguru.png" class="img-fluid" style="margin: 0 auto;">
            <br><br>
            <h1 class="text-center">Administrador</h1>       
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 8rem;">
          <?php include("assets/include/footer.php"); ?>
        </div>
    </div> <!-- .content -->
  </div><!-- /#right-panel -->

  <!-- Right Panel -->

  <?php include("assets/include/script.php"); ?>

</body>
</html>
