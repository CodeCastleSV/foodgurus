<?php
    session_start();

    if(!isset($_SESSION['user_session']))
    {
     header("Location: login.php");
    } else {
        $user_id = $_SESSION['user_id'];
    }

    include_once "../config/database.php";
    include_once "../objetcs/user.php";
    include_once "../objetcs/sliderimage.php";

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);
    $sliderimage = new SliderImage($db);

    $user->user_id = $user_id;
    $user->getUser();

    $value = "nSliderImage";
    $input = "";

    if (isset($_GET['ID'])) {
        $sliderImageID = isset($_GET['ID']) ? $_GET['ID'] : die('ERROR: Imagen Producto ID not found.');
        $input = "<input type='hidden' name='ID' value='{$sliderImageID}' />";
        $option         = isset($_GET['opt']) ? $_GET['opt'] : die('ERROR: Option not found.');
        $value          = $option == "mSliderImage" ? "mSliderImage" : "nSliderImage";
        $sliderimage->ID = $sliderImageID;
        $sliderimage->readOne();
    }

 ?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrador Food Gurus</title>
    <meta name="description" content="Food Gurus Agencia Gastronomica Administrador de Sitio web">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="images/fg.png">

    <!-- Stylesheet -->
    <?php include("assets/include/stylesheet.php"); ?>

    <!-- CKeditor -->
    <script src="assets/libraries/ckeditor/ckeditor.js"></script>

</head>
<body>
  
  <!-- Left Panel -->
  <?php include ("assets/include/navbar-left.php"); ?>
  <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
      <!-- Header -->
      <?php include("assets/include/header.php"); ?>
      <!-- Header -->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Dashboard</a></li>
                            <li><a href="newSliderImage.php">Imagen Slider</a></li>
                            <li class="active">Imagen</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <strong>Imagen</strong> Slider
                      </div>
                      <div class="card-body card-block">
                        <form action="#" id="sliderImageForm" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="opt" value="<?php echo $value; ?>">
                        <?php echo $input; ?>
                          <div class="row form-group">
                            <div class="col col-md-3">
                              <label for="text-input" class=" form-control-label">Nombre de la Imagen</label>
                            </div>
                            <div class="col-12 col-md-6">
                              <input type="text" id="si_name" name="si_name" placeholder="Nombre" class="form-control" value="<?php echo htmlspecialchars($sliderimage->si_name, ENT_QUOTES); ?>">
                              <small class="form-text text-muted">Escriba un nombre de referencia a la imagen</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                              <label for="file-input" class=" form-control-label">Imagen</label>
                            </div>
                            <div class="col-12 col-md-9">
                              <?php
                                if ($sliderimage->si_image) {
                                  echo "<img src='../{$sliderimage->si_image}' style='width:400px;height:250px;'/><br>";
                                  echo "<input type='hidden' name='oldImg' value='{$sliderimage->si_image}' /><br>";
                                }
                              ?>
                              <input type="file" id="si_image" name="si_image" class="form-control-file">
                              <small class="form-text text-muted">Tamaño mínimo del Archivo recomendado: 1366 x 768 pixeles. Formato PNG o JPG. Menor a 5Mb.</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                              <label for="textarea-input" class=" form-control-label">Descripción Cover</label>
                            </div>
                            <div class="col-12 col-md-9">
                              <textarea name="si_description" id="si_description" rows="9" class="form-control ckeditor" >
                                <?php echo htmlspecialchars($sliderimage->si_description, ENT_QUOTES); ?>
                              </textarea>
                              <small class="form-text text-muted">El contenido de este campo se mostrará en el cover del slider.</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">¿Mostrar Imagen en el Slider?</label></div>
                            <div class="col col-md-9">
                              <div class="form-check-inline form-check">
                                <?php
                                  if ($sliderimage->si_status==1) {
                                    echo "<label for='inline-radio1' class='form-check-label'>";
                                    echo "<input type='radio' id='si_status' name='si_status' value='1' class='form-check-input' checked >Si ";
                                    echo "</label>&nbsp;&nbsp;";
                                    echo "<label for='inline-radio2' class='form-check-label'>";
                                    echo "<input type='radio' id='si_status' name='si_status' value='0' class='form-check-input'>No ";
                                    echo "</label>";
                                  }else {
                                    echo "<label for='inline-radio1' class='form-check-label'>";
                                    echo "<input type='radio' id='si_status' name='si_status' value='1' class='form-check-input'>Si ";
                                    echo "</label>&nbsp;&nbsp;";
                                    echo "<label for='inline-radio2' class='form-check-label'>";
                                    echo "<input type='radio' id='si_status' name='si_status' value='0' class='form-check-input' checked >No ";
                                    echo "</label>";
                                  }
                                ?>
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-12">
                              <div class="msg">
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-8">
                              <a href="recordSliderImages.php" class="btn btn-secondary">Cancelar</a>
                            </div>
                            <div class="col col-md-4">
                              <button type="reset" class="btn btn-secondary">
                                <i class="fa fa-ban"></i> Limpiar
                              </button>
                              <button type="submit" class="btn btn-info">
                                <i class="fa fa-save"></i>
                                 <?php echo $value == "nSliderImage" ? "Guardar Imagen" : "Actualizar Imagen"; ?>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

        <?php include("assets/include/footer.php"); ?>
        
    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
    
    <script src="assets/js/app.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/parsley.min.js" type="text/javascript"></script>
    <script src="assets/js/select2.min.js" type="text/javascript"></script>
    <script src="assets/js/app-form-elements.js" type="text/javascript"></script>



    <!-- JQuery Add & Update data -->
    <script type="text/javascript">

      $(document).ready(function(){
          //initialize the javascript
          App.init();
          $('form').parsley();
          App.formElements();

          $("#sliderImageForm").on('submit',(function(e) {
              e.preventDefault();
              
              for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
              }

              $.ajax({
                  type : 'POST',
                  url  : '../objetcs/action.php',
                  data : new FormData(this),
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function(){
                      if ($("#si_name").val() == "") {
                          message = "Por favor ingrese el nombre de la imagen.";
                          error = true;
                      } else if($("#si_description").val() == "") {
                          message = "Por favor escriba la reseña que aparecerá en el cover del slider.";
                          error = true;
                      } else if($("#si_status").val() == "") {
                          message = "Por favor confirme si desea mostrar la imagen.";
                          error = true;
                      } else {
                          error = false;
                      }

                      if (error == true){
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Warning!</strong> '+message+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          return false;
                      }
                  },
                  success :  function(response)   {
                      var parsed = JSON.parse(response);
                      if(parsed.title=="Success"){
                          //$("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Signing In ...');
                          var msg = '<div class="alert alert-success alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-check"></span>'+
                                    '<strong>Warning!</strong> '+parsed.text+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          // fade out effect first
                          $(msg).fadeOut('slow', function(){
                              setTimeout(' window.location.href = "recordSliderImages.php"; ',1500);
                          });
                      }else{
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Advertencia!</strong> '+parsed.text+'</div>';
                          $(".msg").append(msg).fadeIn("slow");
                      }
                  }
              });
              return false;
          }));
      });

      [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
        new SelectFx(el);
      } );

        $('.selectpicker').selectpicker;


        $('#menuToggle').on('click', function(event) {
          $('body').toggleClass('open');
        });

        $('.search-trigger').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').addClass('open');
        });

        $('.search-close').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').removeClass('open');
        });

    </script>


</body>
</html>
