<?php
  session_start();

  if(!isset($_SESSION['user_session']))
  {
   header("Location: login.php");
  } else {
      $user_id = $_SESSION['user_id'];
  }

  include_once "../config/database.php";
  include_once "../objetcs/user.php";
  include_once "../objetcs/ourrecipe.php";

  $database = new Database();
  $db = $database->getConnection();

  $user = new User($db);
  $ourrecipe = new OurRecipe($db);

  $user->user_id = $user_id;
  $user->getUser();

  $value = "nOurRecipe";
  $input = "";

  if (isset($_GET['ID'])) {
      $ourRecipeID = isset($_GET['ID']) ? $_GET['ID'] : die('ERROR: Customer Project ID not found.');
      $input = "<input type='hidden' name='ID' value='{$ourRecipeID}' />";
      $option         = isset($_GET['opt']) ? $_GET['opt'] : die('ERROR: Option not found.');
      $value          = $option == "mOurRecipe" ? "mOurRecipe" : "nOurRecipe";
      $ourrecipe->ID = $ourRecipeID;
      $ourrecipe->readOne();
  }

?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrador Food Gurus</title>
    <meta name="description" content="Food Gurus Agencia Gastronomica Administrador de Sitio web">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="images/fg.png">

    <!-- Stylesheet -->
    <?php include("assets/include/stylesheet.php"); ?>

    <!-- CKeditor -->
    <script src="assets/libraries/ckeditor/ckeditor.js"></script>

</head>
<body>
  
  <!-- Left Panel -->
  <?php include ("assets/include/navbar-left.php"); ?>
  <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">
      <!-- Header -->
      <?php include("assets/include/header.php"); ?>
      <!-- Header -->
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="index.php">Dashboard</a></li>
                            <li><a href="newOurRecipe.php">Nuestra Receta</a></li>
                            <li class="active">Servicio</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-header">
                        <strong>Servicios</strong> Nuestra Receta
                      </div>
                      <div class="card-body card-block">
                        <form action="#" id="ourRecipeForm" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="opt" value="<?php echo $value; ?>">
                        <?php echo $input; ?>
                          <div class="row form-group">
                            <div class="col col-md-3">
                              <label for="text-input" class=" form-control-label">Nombre del Servicio</label>
                            </div>
                            <div class="col-12 col-md-6">
                              <input type="text" id="or_name" name="or_name" placeholder="Nombre del Servicio" class="form-control" value="<?php echo htmlspecialchars($ourrecipe->or_name, ENT_QUOTES); ?>">
                              <small class="form-text text-muted">Escriba un nombre del Servicio</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                              <label for="file-input" class=" form-control-label">Imagen</label>
                            </div>
                            <div class="col-12 col-md-9">
                              <?php
                                if ($ourrecipe->or_image) {
                                  echo "<img src='../{$ourrecipe->or_image}' style='width:500px;height:300px;'/><br>";
                                  echo "<input type='hidden' name='oldImg' value='{$ourrecipe->or_image}' /><br>";
                                }
                              ?>
                              <input type="file" id="or_image" name="or_image" class="form-control-file">
                              <small class="form-text text-muted">Tamaño mínimo del Archivo recomendado: 1366 x 768 pixeles. Formato PNG o JPG. Menor a 5Mb.</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                              <label for="textarea-input" class=" form-control-label">Descripción del Servicio</label>
                            </div>
                            <div class="col-12 col-md-9">
                              <textarea name="or_description" id="or_description" rows="9" placeholder="Content..." class="form-control">
                              <?php echo $ourrecipe->or_description; ?>
                              </textarea>
                              <script>
                                  // Replace the <textarea id="si_description"> with a CKEditor
                                  // instance, using default configuration.
                                  //CKEDITOR.replace('or_description');

                                  CKEDITOR.replace( 'or_description' );

                              </script>
                              <small class="form-text text-muted">El contenido de este campo se mostrará en el mensaje popup del slider correspondiente a la imagen del servicio agregado.</small>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">¿Activar Servicio?</label></div>
                            <div class="col col-md-9">
                              <div class="form-check-inline form-check">
                                <?php
                                  if ($ourrecipe->or_status==1) {
                                    echo "<label for='inline-radio1' class='form-check-label'>";
                                    echo "<input type='radio' id='or_status' name='or_status' value='1' class='form-check-input' checked >Si ";
                                    echo "</label>&nbsp;&nbsp;";
                                    echo "<label for='inline-radio2' class='form-check-label'>";
                                    echo "<input type='radio' id='or_status' name='or_status' value='0' class='form-check-input'>No ";
                                    echo "</label>";
                                  }else {
                                    echo "<label for='inline-radio1' class='form-check-label'>";
                                    echo "<input type='radio' id='or_status' name='or_status' value='1' class='form-check-input'>Si ";
                                    echo "</label>&nbsp;&nbsp;";
                                    echo "<label for='inline-radio2' class='form-check-label'>";
                                    echo "<input type='radio' id='or_status' name='or_status' value='0' class='form-check-input' checked >No ";
                                    echo "</label>";
                                  }
                                ?>
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-12">
                              <div class="msg">
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-8">
                              <a href="recordOurRecipe.php" class="btn btn-secondary">Cancelar</a>
                            </div>
                            <div class="col col-md-4">
                              <button type="reset" class="btn btn-secondary">
                                <i class="fa fa-ban"></i> Limpiar
                              </button>
                              <button type="submit" class="btn btn-info">
                                <i class="fa fa-save"></i>
                                 <?php echo $value == "nOurRecipe" ? "Guardar Servicio" : "Actualizar Servicio"; ?>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

      <?php include("assets/include/footer.php"); ?>

    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js" type="text/javascript"></script>
    
    <script src="assets/js/app.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/parsley.min.js" type="text/javascript"></script>
    <script src="assets/js/select2.min.js" type="text/javascript"></script>
    <script src="assets/js/app-form-elements.js" type="text/javascript"></script>



    <!-- JQuery Add & Update data -->
    <script type="text/javascript">

      $(document).ready(function(){
          //initialize the javascript
          App.init();
          $('form').parsley();
          App.formElements();

          $("#ourRecipeForm").on('submit',(function(e) {
              e.preventDefault();

              for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
              }

              $.ajax({
                  type : 'POST',
                  url  : '../objetcs/action.php',
                  data : new FormData(this),
                  contentType: false,
                  cache: false,
                  processData:false,
                  beforeSend: function(){
                      if ($("#or_name").val() == "") {
                          message = "Por favor ingrese el nombre del servicio.";
                          error = true;
                      } else if($("#or_description").val() == "") {
                          message = "Por favor escriba la descripción del servicio.";
                          error = true;
                      } else if($("#or_status").val() == "") {
                          message = "Por favor confirme si desea mostrar el servicio.";
                          error = true;
                      } else {
                          error = false;
                      }

                      if (error == true){
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Warning!</strong> '+message+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          return false;
                      }
                  },
                  success :  function(response)   {
                      var parsed = JSON.parse(response);
                      if(parsed.title=="Success"){
                          //$("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Signing In ...');
                          var msg = '<div class="alert alert-success alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-check"></span>'+
                                    '<strong>Warning!</strong> '+parsed.text+'.</div>';
                          $(".msg").append(msg).fadeIn("slow");
                          // fade out effect first
                          $(msg).fadeOut('slow', function(){
                              setTimeout(' window.location.href = "recordOurRecipe.php"; ',1500);
                          });
                      }else{
                          var msg = '<div class="alert alert-warning alert-dismissible" role="alert">'+
                                    '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="s7-close"></span></button>'+
                                    '<span class="icon s7-attention"></span>'+
                                    '<strong>Advertencia!</strong> '+parsed.text+'</div>';
                          $(".msg").append(msg).fadeIn("slow");
                      }
                  }
              });
              return false;
          }));
      });

      [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
        new SelectFx(el);
      } );

        $('.selectpicker').selectpicker;


        $('#menuToggle').on('click', function(event) {
          $('body').toggleClass('open');
        });

        $('.search-trigger').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').addClass('open');
        });

        $('.search-close').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          $('.search-trigger').parent('.header-left').removeClass('open');
        });

    </script>



</body>
</html>
