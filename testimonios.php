<div class="row margin-padding-none bg-item-testimonio-1">
			<div class="overlay-dark"></div>
			<div class="overlay-dark col-md-4">
				<div class="row">
					<div class="col-10 offset-1 col-md-4 offset-md-4 img-testimonio-div">
						<img src="images/logos/logo63.png" class="img-fluid rounded-circle" alt="testimonio">
					</div>
					<div class="col-10 offset-2 col-md-9">
						<h5 class="text-center">Kim Axel Lopdrup</h5>
						<h6 class="text-center">CEO Red Lobster</h6>
						<br>
						<p class="text-center"><i>"I got to know Juan when he lived in Guatemala. He impressed me with his hospitality expertise, knowledge of Central American markets and his personal leadership. It is easy to see why his services are in high demand."</i></p>
						<p class="text-center"><i>"Conocí a Juan cuando vivió en Guatemala. Me impresionó con su experiencia en hospitalidad, conocimiento de los mercados centroamericanos y su liderazgo personal.Es fácil entender por qué sus servicios son de alta demanda."</i></p>
						<br>
					</div>
				</div>
			</div>
			<div class="overlay-dark col-md-4">
				<div class="row">
					<div class="col-10 offset-1 col-md-4 offset-md-4 img-testimonio-div">
						<img src="images/logos/logo60.png" class="img-fluid rounded-circle" alt="testimonio">
					</div>
					<div class="col-10 offset-2 col-md-9">
						<h5 class="text-center">Raul Rodriguez</h5>
						<h6 class="text-center">Presidente Grupo Lorena</h6>
						<br>
						<p class="text-center"><i>"Nuestra experiencia de trabajo con Food Gurus ha sido muy satisfactoria. Su experiencia y profesionalismo, han aportado mucho al crecimiento de todas las empresas de Grupo Lorena. Realmente estamos satisfechos con todos los servicios que prestan y especialmente con los resultados."</i></p>
						<br>
					</div>
				</div>
			</div>
			<div class="overlay-dark col-md-4">
				<div class="row">
					<div class="col-10 offset-1 col-md-4 offset-md-4 img-testimonio-div">
						<img src="images/logos/logo23.png" class="img-fluid rounded-circle" alt="testimonio">
					</div>
					<div class="col-10 offset-2 col-md-9">
						<h5 class="text-center">Alejandro Carias</h5>
						<h6 class="text-center">Restaurante Lobby</h6>
						<br>
						<p class="text-center"><i>"Juan es una persona muy involucrada en el rubro y el proceso fue muy fluído. En especifico, su conocimiento y sus contactos nos ahorraron mucho tiempo con temas de permisos y mucho dinero en el tema de equipos. Puedo decir con certeza que la inversión valió mucho la pena."</i></p>
						<br>
					</div>
				</div>
			</div>
			<div class="overlay-dark col-md-4">
				<div class="row">
					<div class="col-10 offset-1 col-md-4 offset-md-4 img-testimonio-div">
						<img src="images/logos/logo61.png" class="img-fluid rounded-circle" alt="testimonio">
					</div>
					<div class="col-10 offset-2 col-md-9">
						<h5 class="text-center">Enrique Font</h5>
						<h6 class="text-center">Director Ejecutivo Grupo Buen Rollo-Guat. Skillets, Tapas & Cañas, Go Green, El Pinche & Dairy Queen</h6>
						<br>
						<p class="text-center"><i>"Juan Matamoros tiene amplia experiencia en la industria de restaurantes, por muchos años diría no hay nadie que sepa más de la industria en Centroamerica."</i></p>
						<br>
					</div>
				</div>
			</div>
			<div class="overlay-dark col-md-4">
				<div class="row">
					<div class="col-10 offset-1 col-md-4 offset-md-4 img-testimonio-div">
						<img src="images/logos/logo58.png" class="img-fluid rounded-circle" alt="testimonio">
					</div>
					<div class="col-10 offset-2 col-md-9">
						<h5 class="text-center">Margarita de Romero</h5>
						<h6 class="text-center">Directora de Alimentos Grupo Campestre</h6>
						<br>
						<p class="text-center"><i>"Tenemos el gusto y la oportunidad de trabajar en equipo con Juan Carlos Matamoros y su empresa Food Gurus desde hace 10 años, en los cuales , Nuestra Marca Pollo Campestre ha crecido y se ha expandido por todo el Pais. Hemos logrado un exitoso cambio de imagen y construido estrategias que nos han permitido ser una de las marcas con mayor crecimiento y competitivas a nivel nacional."</i></p>
						<br>
					</div>
				</div>
			</div>
			<div class="overlay-dark col-md-4">
				<div class="row">
					<div class="col-10 offset-1 col-md-4 offset-md-4 img-testimonio-div">
						<img src="images/logos/logo57.png" class="img-fluid rounded-circle" alt="testimonio">
					</div>
					<div class="col-10 offset-2 col-md-9">
						<h5 class="text-center">Alfonso Ramirez</h5>
						<h6 class="text-center">Director General Premium Restaurant Brands- Mexico</h6>
						<br>
						<p class="text-center"><i>"Conozco a Juan Carlos Matamoros desde hace 25 años, En el ámbito profesional Juan Carlos es un profesional de primer nivel con mucha experiencia en el área de marketing y con un sentido comercial y de negocios superior al promedio lo cual le permite identificar con facilidad las variables que mueven los resultados."</i></p>
						<br>
					</div>
				</div>
			</div>
		</div>