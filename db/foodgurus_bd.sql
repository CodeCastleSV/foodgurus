-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-07-2018 a las 18:38:44
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `foodgurus_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_about_us`
--

CREATE TABLE `fg_about_us` (
  `ID` int(10) NOT NULL,
  `au_section` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Information about Us section';

--
-- Volcado de datos para la tabla `fg_about_us`
--

INSERT INTO `fg_about_us` (`ID`, `au_section`) VALUES
(1, '<p>Desde hace m&aacute;s de 11 a&ntilde;os contribuimos al desarrollo gastron&oacute;mico en la regi&oacute;n Centroamericana y el Caribe, fundada por&nbsp;<span style=\"color:null\"><strong>Juan Matamoros</strong>.</span></p>\r\n\r\n<p>En Food Gurus proveemos soluciones integrales a sus necesidades gastron&oacute;micas, ya sea desarrollando conceptos desde sus inicios o bien asisti&eacute;ndole en la mejora de su actual proyecto en cualquiera de las etapas del mismo.</p>\r\n\r\n<p>Compuesta por personas que ofrecen una interesante mezcla de ideas actuales pero templadas por la experiencia; personas que creemos son las mejores y m&aacute;s experimentadas en su campo dentro de la regi&oacute;n. Como empresa, incentivamos un libre flujo de ideas y un estado constante de cr&iacute;tica constructiva. Esta atm&oacute;sfera ha generado soluciones creativas, conceptos innovadores y muchos clientes satisfechos a trav&eacute;s de los a&ntilde;os.</p>\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_customer_project`
--

CREATE TABLE `fg_customer_project` (
  `ID` int(10) NOT NULL,
  `cp_name` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `cp_image` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `cp_status` int(1) NOT NULL,
  `cp_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Informaction about Customer''s Projects';

--
-- Volcado de datos para la tabla `fg_customer_project`
--

INSERT INTO `fg_customer_project` (`ID`, `cp_name`, `cp_image`, `cp_status`, `cp_created_at`) VALUES
(2, 'SeÃ±or Tenedor', 'admin/images/customer_projects/cp_1532553274.png', 1, '2018-07-25 21:14:34'),
(3, 'Topeca Coffee Roasters', 'admin/images/customer_projects/cp_1532553671.png', 1, '2018-07-25 21:21:11'),
(4, 'Skillets', 'admin/images/customer_projects/cp_1532553753.png', 1, '2018-07-25 21:22:33'),
(5, 'DQ', 'admin/images/customer_projects/cp_1532553891.png', 1, '2018-07-25 21:24:51'),
(6, 'Macaroni Grill', 'admin/images/customer_projects/cp_1532554040.png', 1, '2018-07-25 21:27:20'),
(7, 'Beard Papa\'s', 'admin/images/customer_projects/cp_1532554139.png', 1, '2018-07-25 21:28:59'),
(8, 'Caliche', 'admin/images/customer_projects/cp_1532554167.png', 1, '2018-07-25 21:29:27'),
(9, 'Viva Espresso', 'admin/images/customer_projects/cp_1532555087.png', 1, '2018-07-25 21:30:04'),
(10, 'Donkeys', 'admin/images/customer_projects/cp_1532556264.png', 1, '2018-07-25 21:30:37'),
(12, 'T&C', 'admin/images/customer_projects/cp_1532556096.png', 1, '2018-07-25 21:54:13'),
(13, 'Lilian', 'admin/images/customer_projects/cp_1532556539.png', 1, '2018-07-25 22:08:59'),
(14, 'Fontana', 'admin/images/customer_projects/cp_1532556635.png', 1, '2018-07-25 22:10:35'),
(15, 'Hotel Villa', 'admin/images/customer_projects/cp_1532556672.png', 1, '2018-07-25 22:11:12'),
(16, 'Epic Sandwich Shop', 'admin/images/customer_projects/cp_1532556720.png', 1, '2018-07-25 22:12:00'),
(17, 'Don Beto', 'admin/images/customer_projects/cp_1532556760.png', 1, '2018-07-25 22:12:40'),
(18, 'Pepe & Rony Pizza', 'admin/images/customer_projects/cp_1532556813.png', 1, '2018-07-25 22:13:33'),
(19, 'Bennigan\'s', 'admin/images/customer_projects/cp_1532556887.png', 1, '2018-07-25 22:14:47'),
(20, 'Cadejo', 'admin/images/customer_projects/cp_1532556920.png', 1, '2018-07-25 22:15:20'),
(21, 'Chillis', 'admin/images/customer_projects/cp_1532556967.png', 1, '2018-07-25 22:16:07'),
(22, 'China Wok', 'admin/images/customer_projects/cp_1532556987.png', 1, '2018-07-25 22:16:27'),
(23, 'Caminito', 'admin/images/customer_projects/cp_1532558773.png', 1, '2018-07-25 22:46:13'),
(24, 'Circulo Deportivo Internacional', 'admin/images/customer_projects/cp_1532558807.png', 1, '2018-07-25 22:46:47'),
(25, 'Lobby', 'admin/images/customer_projects/cp_1532558840.png', 1, '2018-07-25 22:47:20'),
(26, 'Hotel El PacÃ­fico', 'admin/images/customer_projects/cp_1532558885.png', 1, '2018-07-25 22:48:05'),
(27, 'El Pinche', 'admin/images/customer_projects/cp_1532558947.png', 1, '2018-07-25 22:49:07'),
(28, 'Fresko', 'admin/images/customer_projects/cp_1532559017.png', 1, '2018-07-25 22:50:17'),
(29, 'Go Green', 'admin/images/customer_projects/cp_1532559047.png', 1, '2018-07-25 22:50:47'),
(30, 'Bexcafe', 'admin/images/customer_projects/cp_1532559070.png', 1, '2018-07-25 22:51:10'),
(31, 'KFC', 'admin/images/customer_projects/cp_1532559092.png', 1, '2018-07-25 22:51:32'),
(32, 'KOI', 'admin/images/customer_projects/cp_1532559122.png', 1, '2018-07-25 22:52:02'),
(33, 'Le Croissant', 'admin/images/customer_projects/cp_1532559159.png', 1, '2018-07-25 22:52:39'),
(34, 'TUMBAO', 'admin/images/customer_projects/cp_1532559175.png', 1, '2018-07-25 22:52:55'),
(35, 'Tartaleta CafÃ© & Bistro', 'admin/images/customer_projects/cp_1532559250.png', 1, '2018-07-25 22:54:10'),
(36, 'Buffalo Wings', 'admin/images/customer_projects/cp_1532559282.png', 1, '2018-07-25 22:54:42'),
(37, 'Crepelovers', 'admin/images/customer_projects/cp_1532559303.png', 1, '2018-07-25 22:55:03'),
(38, 'La NeverÃ­a', 'admin/images/customer_projects/cp_1532559392.png', 1, '2018-07-25 22:56:32'),
(39, 'GBC Burgers', 'admin/images/customer_projects/cp_1532559459.png', 1, '2018-07-25 22:57:39'),
(40, 'Lorena PastelerÃ­a y PanaderÃ­a', 'admin/images/customer_projects/cp_1532559546.png', 1, '2018-07-25 22:59:06'),
(41, 'Pastaria', 'admin/images/customer_projects/cp_1532559596.png', 1, '2018-07-25 22:59:56'),
(42, 'Paradise', 'admin/images/customer_projects/cp_1532559674.png', 1, '2018-07-25 23:01:14'),
(43, 'P.F Chang\'s', 'admin/images/customer_projects/cp_1532559703.png', 1, '2018-07-25 23:01:43'),
(44, 'Hacienda Real', 'admin/images/customer_projects/cp_1532559764.png', 1, '2018-07-25 23:02:44'),
(45, 'Puerto Marisco', 'admin/images/customer_projects/cp_1532559839.png', 1, '2018-07-25 23:03:59'),
(46, 'Quiznos Sub', 'admin/images/customer_projects/cp_1532559859.png', 1, '2018-07-25 23:04:19'),
(47, 'San MartÃ­n', 'admin/images/customer_projects/cp_1532559885.png', 1, '2018-07-25 23:04:45'),
(48, 'Segafredo', 'admin/images/customer_projects/cp_1532559907.png', 1, '2018-07-25 23:05:07'),
(49, 'Los Cebollines', 'admin/images/customer_projects/cp_1532559936.png', 1, '2018-07-25 23:05:36'),
(50, 'TGI FRIDAYS', 'admin/images/customer_projects/cp_1532559980.png', 1, '2018-07-25 23:06:20'),
(51, 'TÃ­picos Margoth', 'admin/images/customer_projects/cp_1532560015.png', 1, '2018-07-25 23:06:55'),
(52, 'Tony Roma\'s', 'admin/images/customer_projects/cp_1532560039.png', 1, '2018-07-25 23:07:19'),
(53, 'Pollo Campestre', 'admin/images/customer_projects/cp_1532560060.png', 1, '2018-07-25 23:07:40'),
(54, 'Casa Ku-ku', 'admin/images/customer_projects/cp_1532560086.png', 1, '2018-07-25 23:08:06'),
(55, 'Mai Thai', 'admin/images/customer_projects/cp_1532560108.png', 1, '2018-07-25 23:08:28'),
(56, 'El Lomo y La Aguja', 'admin/images/customer_projects/cp_1532560129.png', 1, '2018-07-25 23:08:49'),
(57, 'PÃ­zza Hut', 'admin/images/customer_projects/cp_1532560148.png', 1, '2018-07-25 23:09:08'),
(58, 'Wendy\'s', 'admin/images/customer_projects/cp_1532560173.png', 1, '2018-07-25 23:09:33'),
(59, 'UrbÃ¡nica', 'admin/images/customer_projects/cp_1532560350.png', 1, '2018-07-25 23:12:30'),
(60, 'Charleys Philly Steak', 'admin/images/customer_projects/cp_1532560418.png', 1, '2018-07-25 23:13:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_international_alliance`
--

CREATE TABLE `fg_international_alliance` (
  `ID` int(10) NOT NULL,
  `ia_section` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Information about International Alliance section';

--
-- Volcado de datos para la tabla `fg_international_alliance`
--

INSERT INTO `fg_international_alliance` (`ID`, `ia_section`) VALUES
(1, '<p>Somos parte de&nbsp;<strong>Gastrotec</strong>&nbsp;la empresa de&nbsp;<strong>asesor&iacute;a gastron&oacute;mica m&aacute;s grande de Latinoam&eacute;rica</strong>&nbsp;con oficinas en Santiago de Chile, Sao Paulo y San Salvador, especialistas en el desarrollo de facilidades gastron&oacute;micas como mineras, hospitales, hoteles, plantas de procesamiento, cadenas de restaurantes, aeropuertos, supermercados y lavander&iacute;as industriales. Con m&aacute;s de 700 proyectos desarrollados a nivel mundial. M&aacute;s informaci&oacute;n en&nbsp;<a href=\"http://www.gastrotec.cl/\" target=\"_blank\" title=\"Para visitar www.gastrotec.cl haz click aquÃ­\"><strong>www.gastrotec.cl</strong></a></p>\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_our_recipe`
--

CREATE TABLE `fg_our_recipe` (
  `ID` int(10) NOT NULL,
  `or_name` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `or_image` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `or_description` text COLLATE latin1_spanish_ci NOT NULL,
  `or_status` int(1) NOT NULL,
  `or_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Information about Our Recipes items (services)';

--
-- Volcado de datos para la tabla `fg_our_recipe`
--

INSERT INTO `fg_our_recipe` (`ID`, `or_name`, `or_image`, `or_description`, `or_status`, `or_created_at`) VALUES
(2, 'EvaluaciÃ³n', 'admin/images/our_recipes/or_1532640229.jpg', '<ul>\r\n	<li><span style=\"font-size:20px\">Test Conceptos</span></li>\r\n	<li><span style=\"font-size:20px\">Investigaciones Cualitativas y Cuantitativas (focus groups y estudios de campo)</span></li>\r\n	<li><span style=\"font-size:20px\">Evaluaciones Financieras Capex/ROI/VAN/TIR/NUTS</span></li>\r\n	<li><span style=\"font-size:20px\">&iquest;Qu&eacute; concepto funciona para mi mercado?</span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', 1, '2018-07-26 21:23:49'),
(3, 'ConceptualizaciÃ³n', 'admin/images/our_recipes/or_1532646325.jpg', '<ul>\r\n	<li><span style=\"font-size:20px\">Brainstorming</span></li>\r\n	<li><span style=\"font-size:20px\">Sesiones de Ideas</span></li>\r\n	<li><span style=\"font-size:20px\">Big Idea: Tendencias, Concepto, Mercado, Necesidades vrs. Lo Esperado</span></li>\r\n	<li><span style=\"font-size:20px\">Presentaci&oacute;n de opciones en base a realidad de mercado</span></li>\r\n</ul>\r\n', 1, '2018-07-26 23:05:25'),
(4, 'Desarrollo', 'admin/images/our_recipes/or_1532646416.jpg', '<ul>\r\n	<li><span style=\"font-size:20px\">Naming</span></li>\r\n	<li><span style=\"font-size:20px\">Estrategia</span></li>\r\n	<li><span style=\"font-size:20px\">Dise&ntilde;o Gr&aacute;fico y Empaques</span></li>\r\n	<li><span style=\"font-size:20px\">Dise&ntilde;o de Cocinas-Flujograma</span></li>\r\n	<li><span style=\"font-size:20px\">Dise&ntilde;o Arquitect&oacute;nico</span></li>\r\n	<li><span style=\"font-size:20px\">Look &amp; Feel (Moodboard)</span></li>\r\n	<li><span style=\"font-size:20px\">Desarrollo y recomendaci&oacute;n recetas</span></li>\r\n</ul>\r\n', 1, '2018-07-26 23:06:56'),
(5, 'ImplementaciÃ³n', 'admin/images/our_recipes/or_1532646466.jpg', '<ul>\r\n	<li><span style=\"font-size:20px\">Compras / Proveedores</span></li>\r\n	<li><span style=\"font-size:20px\">Negociaciones y Recomendaci&oacute;n de mobiliario / equipamiento</span></li>\r\n	<li><span style=\"font-size:20px\">Inversi&oacute;n proyecci&oacute;n</span></li>\r\n	<li><span style=\"font-size:20px\">Desarrollo de Procesos</span></li>\r\n	<li><span style=\"font-size:20px\">Selecci&oacute;n de sitio</span></li>\r\n</ul>\r\n', 1, '2018-07-26 23:07:46'),
(6, 'EjecuciÃ³n', 'admin/images/our_recipes/or_1532646514.jpg', '<ul>\r\n	<li><span style=\"font-size:20px\">Construcci&oacute;n (tercerizada)</span></li>\r\n	<li><span style=\"font-size:20px\">Recetas Pruebas</span></li>\r\n	<li><span style=\"font-size:20px\">Entrenamiento &amp; Capacitaci&oacute;n</span></li>\r\n	<li><span style=\"font-size:20px\">Manualizaci&oacute;n</span></li>\r\n	<li><span style=\"font-size:20px\">Gran Opening</span></li>\r\n</ul>\r\n', 1, '2018-07-26 23:08:34'),
(7, 'Seguimiento', 'admin/images/our_recipes/or_1532646570.jpg', '<ul>\r\n	<li><span style=\"font-size:20px\">Soporte y Apoyo Comercial (Mejora en Ventas)</span></li>\r\n	<li><span style=\"font-size:20px\">Evaluaci&oacute;n Financiera</span></li>\r\n	<li><span style=\"font-size:20px\">Manumodel</span></li>\r\n	<li><span style=\"font-size:20px\">An&aacute;lisis de la Salud de la marca</span></li>\r\n	<li><span style=\"font-size:20px\">Desarrollo de Auditorias: Costos y despercicios, recetas, procesos, compras, inventarios y manejo de bodegas.</span></li>\r\n	<li><span style=\"font-size:20px\">Capacitaciones</span></li>\r\n</ul>\r\n', 1, '2018-07-26 23:09:30'),
(8, 'ExpansiÃ³n', 'admin/images/our_recipes/or_1532646614.jpg', '<ul>\r\n	<li>\r\n     <span style=\"font-size:20px\">Desarrollo de Franquicias</span>\r\n	</li>\r\n	<li><span style=\"font-size:20px\">Diagnostico de Franquiciabilidad</span></li>\r\n	<li><span style=\"font-size:20px\">Comercializaci&oacute;n o adquisici&oacute;n de franquicias internacionales</span></li>\r\n	<li><span style=\"font-size:20px\">Desarrollo e Implementaci&oacute;n de programa de franquicias</span></li>\r\n	<li><span style=\"font-size:20px\">Manualizaci&oacute;n</span></li>\r\n</ul>\r\n', 1, '2018-07-26 23:10:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_personal_testimony`
--

CREATE TABLE `fg_personal_testimony` (
  `ID` int(10) NOT NULL,
  `pt_name` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `pt_image` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `pt_job` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `pt_description` text COLLATE latin1_spanish_ci NOT NULL,
  `pt_status` int(1) NOT NULL,
  `pt_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Information about Personal Testimonies of Food Gurus';

--
-- Volcado de datos para la tabla `fg_personal_testimony`
--

INSERT INTO `fg_personal_testimony` (`ID`, `pt_name`, `pt_image`, `pt_job`, `pt_description`, `pt_status`, `pt_created_at`) VALUES
(2, 'Kim Axel Lopdrup', 'admin/images/testimonies/pt_1532711973.png', 'CEO Red Lobster', '<p><em>&quot;I got to know Juan when he lived in Guatemala. He impressed me with his hospitality expertise, knowledge of Central American markets and his personal leadership. It is easy to see why his services are in high demand.&quot;<br />\r\n&quot;Conoc&iacute; a Juan cuando vivi&oacute; en Guatemala. Me impresion&oacute; con su experiencia en hospitalidad, conocimiento de los mercados centroamericanos y su liderazgo personal.Es f&aacute;cil entender por qu&eacute; sus servicios son de alta demanda.&quot;</em></p>\r\n', 1, '2018-07-27 17:19:33'),
(3, 'RaÃºl Rodriguez', 'admin/images/testimonies/pt_1532712547.png', 'Presidente Grupo Lorena', '<p><em>&quot;Nuestra experiencia de trabajo con Food Gurus ha sido muy satisfactoria. Su experiencia y profesionalismo, han aportado mucho al crecimiento de todas las empresas de Grupo Lorena. Realmente estamos satisfechos con todos los servicios que prestan y especialmente con los resultados.&quot;</em></p>\r\n', 1, '2018-07-27 17:29:07'),
(4, 'Alejandro Carias', 'admin/images/testimonies/pt_1532712623.png', 'Restaurante Lobby', '<p><em>&quot;Juan es una persona muy involucrada en el rubro y el proceso fue muy flu&iacute;do. En especifico, su conocimiento y sus contactos nos ahorraron mucho tiempo con temas de permisos y mucho dinero en el tema de equipos. Puedo decir con certeza que la inversi&oacute;n vali&oacute; mucho la pena.&quot;</em></p>\r\n', 1, '2018-07-27 17:30:23'),
(5, 'Enrique Font', 'admin/images/testimonies/pt_1532712688.png', 'Director Ejecutivo Grupo Buen Rollo-Guat. Skillets, Tapas & CaÃ±as, Go Green, El Pinche & Dairy Queen', '<p><em>&quot;Juan Matamoros tiene amplia experiencia en la industria de restaurantes, por muchos a&ntilde;os dir&iacute;a no hay nadie que sepa m&aacute;s de la industria en Centroamerica.&quot;</em></p>\r\n', 1, '2018-07-27 17:31:28'),
(6, 'Margarita de Romero', 'admin/images/testimonies/pt_1532712776.png', 'Directora de Alimentos Grupo Campestre', '<p><em>&quot;Tenemos el gusto y la oportunidad de trabajar en equipo con Juan Carlos Matamoros y su empresa Food Gurus desde hace 10 a&ntilde;os, en los cuales , Nuestra Marca Pollo Campestre ha crecido y se ha expandido por todo el Pais. Hemos logrado un exitoso cambio de imagen y construido estrategias que nos han permitido ser una de las marcas con mayor crecimiento y competitivas a nivel nacional.&quot;</em></p>\r\n', 1, '2018-07-27 17:32:56'),
(7, 'Alfonzo RamÃ­rez', 'admin/images/testimonies/pt_1532712852.png', 'Director General Premium Restaurant Brands - MÃ©xico', '<p><em>&quot;Conozco a Juan Carlos Matamoros desde hace 25 a&ntilde;os, En el &aacute;mbito profesional Juan Carlos es un profesional de primer nivel con mucha experiencia en el &aacute;rea de marketing y con un sentido comercial y de negocios superior al promedio lo cual le permite identificar con facilidad las variables que mueven los resultados.&quot;</em></p>\r\n', 1, '2018-07-27 17:34:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_slider_image`
--

CREATE TABLE `fg_slider_image` (
  `ID` int(10) NOT NULL,
  `si_name` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `si_image` varchar(250) COLLATE latin1_spanish_ci NOT NULL,
  `si_description` text COLLATE latin1_spanish_ci NOT NULL,
  `si_status` int(1) NOT NULL,
  `si_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Information about Slider''s image';

--
-- Volcado de datos para la tabla `fg_slider_image`
--

INSERT INTO `fg_slider_image` (`ID`, `si_name`, `si_image`, `si_description`, `si_status`, `si_created_at`) VALUES
(1, 'Banner 1', 'admin/images/slider_images/imagen_1532128366.jpg', '<h1 style=\"text-align:center\"><span style=\"font-size:48px\"><span style=\"color:#ffffff\">Te ayudamos a</span></span></h1>\r\n\r\n<h2 style=\"text-align:center\"><span style=\"font-size:48px\"><span style=\"color:#f1c40f\">Reducir</span></span></h2>\r\n\r\n<h1 style=\"text-align:center\"><span style=\"font-size:48px\"><span style=\"color:#ffffff\">tus Costos</span></span></h1>\r\n', 1, '2018-07-20 23:12:46'),
(2, 'Banner 2', 'admin/images/slider_images/imagen_1532975403.jpg', '<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">Somos la primera</span></span></h1>\r\n\r\n<h2 style=\"text-align:center\"><span style=\"color:#f1c40f\"><span style=\"font-size:48px\">Agencia Gastron&oacute;mica</span></span></h2>\r\n\r\n<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">en Centroam&eacute;rica</span></span></h1>\r\n', 1, '2018-07-30 18:30:03'),
(3, 'Banner 3', 'admin/images/slider_images/imagen_1532975518.jpg', '<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">Desarrollamos tu</span></span></h1>\r\n\r\n<h2 style=\"text-align:center\"><span style=\"color:#f1c40f\"><span style=\"font-size:48px\">Men&uacute;</span></span></h2>\r\n\r\n<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">y Recetas</span></span></h1>\r\n', 1, '2018-07-30 18:31:58'),
(4, 'Banner 4', 'admin/images/slider_images/imagen_1532975575.jpg', '<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">Dise&ntilde;amos tu</span></span></h1>\r\n\r\n<h2 style=\"text-align:center\"><span style=\"color:#f1c40f\"><span style=\"font-size:48px\">Cocina</span></span></h2>\r\n\r\n<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">y Restaurante</span></span></h1>\r\n', 1, '2018-07-30 18:32:56'),
(5, 'Banner 5', 'admin/images/slider_images/imagen_1532975628.jpg', '<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">Te ayudamos a</span></span></h1>\r\n\r\n<h2 style=\"text-align:center\"><span style=\"color:#f1c40f\"><span style=\"font-size:48px\">Reducir</span></span></h2>\r\n\r\n<h1 style=\"text-align:center\"><span style=\"color:#ffffff\"><span style=\"font-size:48px\">tus Costos</span></span></h1>\r\n', 1, '2018-07-30 18:33:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_users`
--

CREATE TABLE `fg_users` (
  `ID` int(10) NOT NULL,
  `u_name` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `u_lastname` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `u_username` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `u_password` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `u_status` int(1) NOT NULL,
  `u_user_type` int(1) NOT NULL,
  `u_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Information about users';

--
-- Volcado de datos para la tabla `fg_users`
--

INSERT INTO `fg_users` (`ID`, `u_name`, `u_lastname`, `u_username`, `u_password`, `u_status`, `u_user_type`, `u_created_at`) VALUES
(1, 'Juan', 'Matamoros', 'adminFG', '$2y$10$yE4Cw/Oi6hTVsRAHszt2A.uhVUTa3kEXIN6UQVzTxVlLLZ7FpkOHG', 1, 1, '2018-07-30 17:32:00'),
(2, 'Developer', 'Food Gurus', 'developerFG', '$2y$10$2QTDP/Z7Z4GEevWGZy68aOOqwcICtpDZcjVUiRMQHxF8O6YG44yOe', 1, 1, '2018-07-30 17:58:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fg_world_certification`
--

CREATE TABLE `fg_world_certification` (
  `ID` int(10) NOT NULL,
  `wc_section` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci COMMENT='Information about World Certification''s section';

--
-- Volcado de datos para la tabla `fg_world_certification`
--

INSERT INTO `fg_world_certification` (`ID`, `wc_section`) VALUES
(1, '<p>Como miembros de&nbsp;<span style=\"color:null\"><strong>FCSI (Foodservice Consultants Society Internacional)</strong></span>&nbsp;certificamos nuestras capacidades en el &aacute;rea de asesor&iacute;a culinaria, con est&aacute;ndares internacionales. Esto implica cumplir con requisitos, relacionados con nuestra experiencia y tiempo en el mercado, y con la actualizaci&oacute;n constante de los conocimientos y habilidades del equipo de trabajo que conforma Food Gurus. Somos la &uacute;nica compa&ntilde;&iacute;a consultora autorizada para Centroamerica. FCSI es una asociaci&oacute;n con sede en Louisville, KY. Estados Unidos y cuenta con m&aacute;s de 900 miembros, en 35 pa&iacute;ses del mundo.</p>\r\n\r\n<p>Como consultores independientes, NO estamos asociados con la fabricaci&oacute;n o venta de maquinaria, accesorios, equipamientos o comisiones de ninguna clase; por lo mismo, no hemos generado lealtades con ning&uacute;n proveedor espec&iacute;fico de equipos, productos o servicios lo que pueda nublar nuestra visi&oacute;n para hacer recomendaciones siempre en el mejor inter&eacute;s de nuestros clientes.</p>\r\n');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `fg_about_us`
--
ALTER TABLE `fg_about_us`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fg_customer_project`
--
ALTER TABLE `fg_customer_project`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fg_international_alliance`
--
ALTER TABLE `fg_international_alliance`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fg_our_recipe`
--
ALTER TABLE `fg_our_recipe`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fg_personal_testimony`
--
ALTER TABLE `fg_personal_testimony`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fg_slider_image`
--
ALTER TABLE `fg_slider_image`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fg_users`
--
ALTER TABLE `fg_users`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `fg_world_certification`
--
ALTER TABLE `fg_world_certification`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `fg_about_us`
--
ALTER TABLE `fg_about_us`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fg_customer_project`
--
ALTER TABLE `fg_customer_project`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `fg_international_alliance`
--
ALTER TABLE `fg_international_alliance`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fg_our_recipe`
--
ALTER TABLE `fg_our_recipe`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `fg_personal_testimony`
--
ALTER TABLE `fg_personal_testimony`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `fg_slider_image`
--
ALTER TABLE `fg_slider_image`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fg_users`
--
ALTER TABLE `fg_users`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `fg_world_certification`
--
ALTER TABLE `fg_world_certification`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
