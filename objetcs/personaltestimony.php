<?php

class PersonalTestimony
{
    private $conn;
    private $table_name = "fg_personal_testimony";

    /** Slider Image items **/
    public $ID;
    public $pt_name;
    public $pt_image;
    public $pt_job;
    public $pt_description;
    public $pt_status;
    public $pt_created_at;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function readAll() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " ORDER BY pt_created_at DESC";


            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function readOne() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->pt_name   = $row['pt_name'];
            $this->pt_image = $row['pt_image'];
            $this->pt_job = $row['pt_job'];
            $this->pt_description     = $row['pt_description'];
            $this->pt_status = $row['pt_status'];
            $this->pt_created_at = $row['pt_created_at'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createPersonalTestimony(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (pt_name, pt_image, pt_job, pt_description, pt_status, pt_created_at)
                   VALUES (:pt_name, :pt_image, :pt_job, :pt_description, :pt_status, :pt_created_at)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("pt_name", $this->pt_name);
          $stmt->bindParam("pt_image", $this->pt_image);
          $stmt->bindParam("pt_job", $this->pt_job);
          $stmt->bindParam("pt_description", $this->pt_description);
          $stmt->bindParam("pt_status", $this->pt_status);
          $stmt->bindParam("pt_created_at", $this->pt_created_at);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function updatePersonalTestimony(){

        try {
            $query = "UPDATE
                      " . $this->table_name . "
                  SET
                    pt_name = :pt_name,
                    pt_image = :pt_image,
                    pt_job = :pt_job,
                    pt_description = :pt_description,
                    pt_status = :pt_status
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("pt_name", $this->pt_name);
            $stmt->bindParam("pt_image", $this->pt_image);
            $stmt->bindParam("pt_job", $this->pt_job);
            $stmt->bindParam("pt_description", $this->pt_description);
            $stmt->bindParam("pt_status", $this->pt_status);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deletePersonalTestimony() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }

    /** Personal Testimony Items **/

    public function readPersonalTestimony() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name. " WHERE pt_status=1";

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}
?>
