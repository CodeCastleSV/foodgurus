<?php
	// Session Start
	session_start();

	// include database and object files
	include_once '../config/database.php';
	include_once 'user.php';
	include_once 'sliderimage.php';
	include_once 'aboutus.php';
	include_once 'worldcertification.php';
	include_once 'internationalalliance.php';
	include_once 'customerproject.php';
	include_once 'ourrecipe.php';
	include_once 'personaltestimony.php';

	// instantiate database class
	$database = new Database();
	$db = $database->getConnection();

	// initialize objects
	$user = new User($db);
	$sliderimage = new SliderImage($db);
	$aboutus = new AboutUs($db);
	$worldcertification = new WorldCertification($db);
	$internationalalliance = new InternationalAlliance($db);
	$customerproject = new CustomerProject($db);
	$ourrecipe = new OurRecipe($db);
	$personaltestimony = new PersonalTestimony($db);

	// set your default timezone
	date_default_timezone_set('America/El_Salvador');

	if (isset($_POST['opt'])) {
		$option = $_POST['opt'];
	}

	if (isset($_GET['opt'])) {
		$option = $_GET['opt'];
	}

	if ($option == "nLogin") {

		$user->u_username = $_POST['u_username'];
		$user->u_password = $_POST['u_password'];

		if ( $user->doLogin() ) {
			echo json_encode(array("title" => "Success", "text" => "ok", "type" => "{$_SESSION['is_Admin']}"));
		} else {
			echo json_encode(array("title" => "Warning", "text" => "Revisa la informaci&oacute;n proporcionada no coinciden con nuestros registros.", "type" => "warning"));
		}
	}

	/** ImagenSlider **/

	if ($option == "nSliderImage") {

		$sliderimage->si_name = $_POST['si_name'];
		$sliderimage->si_description = $_POST['si_description'];
		$sliderimage->si_status = $_POST['si_status'];

		$imgFile = $_FILES['si_image']['name'];
		$tmp_dir = $_FILES['si_image']['tmp_name'];
		$imgSize = $_FILES['si_image']['size'];

		$upload_dir = '../admin/images/slider_images/'; // upload directory

		$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

		// valid image extensions
		$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
		// rename uploading image
		$bannerSlider = "imagen_" . time() . "." . $imgExt;
		// allow valid image file formats
		if(in_array($imgExt, $valid_extensions)){
			// Check file size '5MB'
			if($imgSize < 5000000)				{
				move_uploaded_file($tmp_dir,$upload_dir.$bannerSlider);
			}	else {
				$errMSG = "Sorry, your file is too large.";
			}
		}	else {
			$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		}

		if(!isset($errMSG)) {
			$sliderimage->si_image = "admin/images/slider_images/".$bannerSlider;
			$sliderimage->si_created_at = date("Y-m-d H:i:s");

			if ($sliderimage->createSliderImage()) {
				echo json_encode(array("title" => "Success", "text" => "Imagen de Slider creada con &eacute;xito", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "mSliderImage") {

		$sliderimage->ID = $_POST['ID'];
		$sliderimage->si_name = $_POST['si_name'];
		$sliderimage->si_description = $_POST['si_description'];
		$sliderimage->si_status = $_POST['si_status'];

		$oldImg = $_POST['oldImg'];

		$imgFile = $_FILES['si_image']['name'];
		$tmp_dir = $_FILES['si_image']['tmp_name'];
		$imgSize = $_FILES['si_image']['size'];

		if ($imgFile) {
			$upload_dir = '../admin/images/slider_images/'; // upload directory

			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

			// valid image extensions
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			// rename uploading image
			$bannerSlider = "imagen_" . time() . "." . $imgExt;
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){
				// Check file size '5MB'
				if($imgSize < 5000000) {
					unlink("../".$oldImg);
					move_uploaded_file($tmp_dir,$upload_dir.$bannerSlider);
				}	else {
					$errMSG = "Sorry, your file is too large.";
				}
			}	else {
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			}
			$sliderimage->si_image = "admin/images/slider_images/".$bannerSlider;
		} else {
			$sliderimage->si_image = $oldImg;
		}

		if(!isset($errMSG)) {
			if ($sliderimage->updateSliderImage()) {
				echo json_encode(array("title" => "Success", "text" => "Imagen de Slider actualizada con &eacute;xito!!", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "eSliderImage") {

		$sliderimage->ID = $_GET['ID'];

		if ($sliderimage->deleteSliderImage()) {
			header("Location: ../admin/recordSliderImages.php");
		} else {
			echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
		}
	}

	/** About Us **/

	if ($option == "nAboutUs") {

		$aboutus->au_section = $_POST['au_section'];

		if(!isset($errMSG)) {
			if ($aboutus->createAboutUs()) {
				echo json_encode(array("title" => "Success", "text" => "Sección creada con éxito", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "mAboutUs") {

		$aboutus->ID = $_POST['ID'];
		$aboutus->au_section = $_POST['au_section'];

		if(!isset($errMSG)) {
			if ($aboutus->updateAboutUs()) {
				echo json_encode(array("title" => "Success", "text" => "Sección actualizada con éxito!!", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "eAboutUs") {

		$aboutus->ID = $_GET['ID'];

		if ($aboutus->deleteAboutUs()) {
			header("Location: ../admin/recordAboutUs.php");
		} else {
			echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
		}
	}

	/** World Certification **/

	if ($option == "nWorldCertification") {

		$worldcertification->wc_section = $_POST['wc_section'];

		if(!isset($errMSG)) {
			if ($worldcertification->createWorldCertification()) {
				echo json_encode(array("title" => "Success", "text" => "Sección creada con éxito", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "mWorldCertification") {

		$worldcertification->ID = $_POST['ID'];
		$worldcertification->wc_section = $_POST['wc_section'];

		if(!isset($errMSG)) {
			if ($worldcertification->updateWorldCertification()) {
				echo json_encode(array("title" => "Success", "text" => "Sección actualizada con éxito!!", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "eWorldCertification") {

		$worldcertification->ID = $_GET['ID'];

		if ($worldcertification->deleteAboutUs()) {
			header("Location: ../admin/recordWorldCertification.php");
		} else {
			echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
		}
	}

	/** International Alliance **/

	if ($option == "nInternationalAlliance") {

		$internationalalliance->ia_section = $_POST['ia_section'];

		if(!isset($errMSG)) {
			if ($internationalalliance->createInternationalAlliance()) {
				echo json_encode(array("title" => "Success", "text" => "Sección creada con éxito", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "mInternationalAlliance") {

		$internationalalliance->ID = $_POST['ID'];
		$internationalalliance->ia_section = $_POST['ia_section'];

		if(!isset($errMSG)) {
			if ($internationalalliance->updateInternationalAlliance()) {
				echo json_encode(array("title" => "Success", "text" => "Sección actualizada con éxito!!", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "eInternationalAlliance") {

		$internationalalliance->ID = $_GET['ID'];

		if ($internationalalliance->deleteInternationalAlliance()) {
			header("Location: ../admin/recordInternationalAlliance.php");
		} else {
			echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
		}
	}

	/** Customer Projects **/

	if ($option == "nCustomerProject") {

		$customerproject->cp_name = $_POST['cp_name'];
		$customerproject->cp_status = $_POST['cp_status'];

		$imgFile = $_FILES['cp_image']['name'];
		$tmp_dir = $_FILES['cp_image']['tmp_name'];
		$imgSize = $_FILES['cp_image']['size'];

		$upload_dir = '../admin/images/customer_projects/'; // upload directory

		$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

		// valid image extensions
		$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
		// rename uploading image
		$customer = "cp_" . time() . "." . $imgExt;
		// allow valid image file formats
		if(in_array($imgExt, $valid_extensions)){
			// Check file size '5MB'
			if($imgSize < 5000000)				{
				move_uploaded_file($tmp_dir,$upload_dir.$customer);
			}	else {
				$errMSG = "Sorry, your file is too large.";
			}
		}	else {
			$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		}

		if(!isset($errMSG)) {
			$customerproject->cp_image = "admin/images/customer_projects/".$customer;
			$customerproject->cp_created_at = date("Y-m-d H:i:s");

			if ($customerproject->createCustomerProject()) {
				echo json_encode(array("title" => "Success", "text" => "Proyecto de Cliente creado con &eacute;xito", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "mCustomerProject") {

		$customerproject->ID = $_POST['ID'];
		$customerproject->cp_name = $_POST['cp_name'];
		$customerproject->cp_status = $_POST['cp_status'];

		$oldImg = $_POST['oldImg'];

		$imgFile = $_FILES['cp_image']['name'];
		$tmp_dir = $_FILES['cp_image']['tmp_name'];
		$imgSize = $_FILES['cp_image']['size'];

		if ($imgFile) {
			$upload_dir = '../admin/images/customer_projects/'; // upload directory

			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

			// valid image extensions
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			// rename uploading image
			$customer = "cp_" . time() . "." . $imgExt;
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){
				// Check file size '5MB'
				if($imgSize < 5000000) {
					unlink("../".$oldImg);
					move_uploaded_file($tmp_dir,$upload_dir.$customer);
				}	else {
					$errMSG = "Sorry, your file is too large.";
				}
			}	else {
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			}
			$customerproject->cp_image = "admin/images/customer_projects/".$customer;
		} else {
			$customerproject->cp_image = $oldImg;
		}

		if(!isset($errMSG)) {
			if ($customerproject->updateCustomerProject()) {
				echo json_encode(array("title" => "Success", "text" => "Proyecto de Cliente actualizado con &eacute;xito!!", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "eCustomerProject") {

		$customerproject->ID = $_GET['ID'];

		if ($customerproject->deleteCustomerProject()) {
			header("Location: ../admin/recordCustomerProjects.php");
		} else {
			echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
		}
	}

	/** Personal Testimony **/

	if ($option == "nPersonalTestimony") {

		$personaltestimony->pt_name = $_POST['pt_name'];
		$personaltestimony->pt_job = $_POST['pt_job'];
		$personaltestimony->pt_description = stripslashes($_POST['pt_description']);
		$personaltestimony->pt_status = $_POST['pt_status'];

		$imgFile = $_FILES['pt_image']['name'];
		$tmp_dir = $_FILES['pt_image']['tmp_name'];
		$imgSize = $_FILES['pt_image']['size'];

		$upload_dir = '../admin/images/testimonies/'; // upload directory

		$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

		// valid image extensions
		$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
		// rename uploading image
		$testimony = "pt_" . time() . "." . $imgExt;
		// allow valid image file formats
		if(in_array($imgExt, $valid_extensions)){
			// Check file size '5MB'
			if($imgSize < 5000000)				{
				move_uploaded_file($tmp_dir,$upload_dir.$testimony);
			}	else {
				$errMSG = "Sorry, your file is too large.";
			}
		}	else {
			$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		}

		if(!isset($errMSG)) {
			$personaltestimony->pt_image = "admin/images/testimonies/".$testimony;
			$personaltestimony->pt_created_at = date("Y-m-d H:i:s");

			if ($personaltestimony->createPersonalTestimony()) {
				echo json_encode(array("title" => "Success", "text" => "Testimonio creado con &eacute;xito", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "mPersonalTestimony") {

		$personaltestimony->ID = $_POST['ID'];
		$personaltestimony->pt_name = $_POST['pt_name'];
		$personaltestimony->pt_job = $_POST['pt_job'];
		$personaltestimony->pt_description = stripslashes($_POST['pt_description']);
		$personaltestimony->pt_status = $_POST['pt_status'];

		$oldImg = $_POST['oldImg'];

		$imgFile = $_FILES['pt_image']['name'];
		$tmp_dir = $_FILES['pt_image']['tmp_name'];
		$imgSize = $_FILES['pt_image']['size'];

		if ($imgFile) {
			$upload_dir = '../admin/images/testimonies/'; // upload directory

			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension

			// valid image extensions
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			// rename uploading image
			$testimony = "or_" . time() . "." . $imgExt;
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){
				// Check file size '5MB'
				if($imgSize < 5000000) {
					unlink("../".$oldImg);
					move_uploaded_file($tmp_dir,$upload_dir.$testimony);
				}	else {
					$errMSG = "Sorry, your file is too large.";
				}
			}	else {
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			}
			$personaltestimony->pt_image = "admin/images/testimonies/".$testimony;
		} else {
			$personaltestimony->pt_image = $oldImg;
		}

		if(!isset($errMSG)) {
			if ($personaltestimony->updatePersonalTestimony()) {
				echo json_encode(array("title" => "Success", "text" => "Testimonio actualizado con &eacute;xito!!", "type" => "success"));
			} else {
				echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
			}
		} else {
			echo json_encode(array("title" => "Error!", "text" => "{$errMSG}", "type" => "error"));
		}
	}

	if ($option == "ePersonalTestimony") {

		$personaltestimony->ID = $_GET['ID'];

		if ($personaltestimony->deletePersonalTestimony()) {
			header("Location: ../admin/recordPersonalTestimonies.php");
		} else {
			echo json_encode(array("title" => "Error", "text" => "Algo salio mal, revise la informacion he intente de nuevo.", "type" => "error"));
		}
	}

?>
