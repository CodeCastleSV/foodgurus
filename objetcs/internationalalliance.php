<?php

class InternationalAlliance
{
    private $conn;
    private $table_name = "fg_international_alliance";

    /** World Certification Section items **/
    public $ID;
    public $ia_section;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function readAll() {

        try {
            $query = "SELECT * FROM " . $this->table_name;


            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function readOne() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->ia_section   = $row['ia_section'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createInternationalAlliance(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (ia_section)
                   VALUES (:ia_section)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("ia_section", $this->ia_section);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function updateInternationalAlliance(){

        try {
            $query = "UPDATE " . $this->table_name . "
                  SET
                    ia_section = :ia_section
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("ia_section", $this->ia_section);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteInternationalAlliance() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }

    /** World Certification Items **/

    public function readInternationalAlliance() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name;

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}
?>
