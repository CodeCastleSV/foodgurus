<?php

class User
{
    private $conn;
    private $table_name = "fg_users";

    //Usuarios
    public $ID;
    public $u_name;
    public $u_lastname;
    public $u_username;
    public $u_password;
    public $u_status;
    public $u_user_type;
    public $u_created_at;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function doLogin() {
        try
        {
            // query to insert record 
            $query = "SELECT * FROM " . $this->table_name . " WHERE  u_username = :u_username";

            // prepare query
            $stmt = $this->conn->prepare($query);
            // bind values
            $stmt->bindParam(":u_username", $this->u_username, PDO::PARAM_STR);
            // execute query
            $stmt->execute();
            $userRow = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                $_SESSION['tpass'] = $this->u_password;
                $_SESSION['upass'] = $userRow['u_password'];
                if (password_verify($this->u_password, $userRow['u_password'])) {
                //if ($this->u_password == $userRow['u_password']) {
                    $_SESSION['user_id']      = $userRow['ID'];
                    $_SESSION['user_session'] = $userRow['u_name'] . "  " . $userRow['u_lastname'];
                    $_SESSION['is_Admin']     = $userRow['u_user_type'];
                    return true;
                } else {
                    return false;
                }
            }
        } catch(PDOException $ex) {
            echo $ex->getMessage();
        }

    }

    public function is_loggedin() {
        if(isset($_SESSION['user_session'])) {
            return true;
        }
    }

    public function redirect($url) {
        header("Location: $url");
    }

    public function logout() {
        session_destroy();
        unset($_SESSION['user_session']);
        return true;
    }

    public function getUser() {
        try {

            $query = "SELECT *
                      FROM " . $this->table_name . " WHERE ID = :ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->ID     = $row['ID'];
            $this->u_name     = $row['u_name'];
            $this->u_lastname     = $row['u_lastname'];
            $this->u_username     = $row['u_username'];
            $this->u_password     = $row['u_password'];
            $this->u_status     = $row['u_status'];
            $this->u_user_type = $row['u_user_type'];

        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function readAll() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name . " ORDER BY u_name ASC";

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    public function readOne() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name . "
                WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->ID     = $row['ID'];
            $this->u_name     = $row['u_name'];
            $this->u_lastname     = $row['u_lastname'];
            $this->u_username     = $row['u_username'];
            $this->u_password     = $row['u_password'];
            $this->u_status     = $row['u_status'];
            $this->u_user_type = $row['u_user_type'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    public function createUser(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (u_name, u_lastname, u_username, u_password, u_status, u_user_type, u_created_at)
                   VALUES (:u_name, :u_lastname, :u_username, :u_password, :u_status, :u_user_type, :u_created_at)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("u_name", $this->u_name);
          $stmt->bindParam("u_lastname", $this->u_lastname);
          $stmt->bindParam("u_username", $this->u_username);
          $stmt->bindParam("u_password", $this->u_password);
          $stmt->bindParam("u_status", $this->u_status);
          $stmt->bindParam("u_user_type", $this->u_user_type);
          $stmt->bindParam("u_created_at", $this->u_created_at);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }

    }

    /*public function registrarse(){

        try {

          $query4 = "SELECT count(u_username) as cuenta FROM " . $this->table_name . " where username=:username";

          $stmt4 = $this->conn->prepare($query4);
          $stmt4->bindParam("username", $this->username);
          if($stmt4->execute()){
            $row2 = $stmt4->fetch(PDO::FETCH_ASSOC);
            $resultado = $row2['cuenta'];
            if ($resultado > 0) {
              return false;
            } else {
              $query = "INSERT INTO " . $this->table_name . " (u_name, u_lastname, username, password, idTipoUsuario, estadoUsuario)
               VALUES (:u_name, :u_lastname, :username, :password, :idTipoUsuario, :estadoUsuario)";

              $stmt = $this->conn->prepare( $query );

              $stmt->bindParam("u_name", $this->u_name);
              $stmt->bindParam("u_lastname", $this->u_lastname);
              $stmt->bindParam("username", $this->username);
              $stmt->bindParam("password", $this->password);
              $stmt->bindParam("idTipoUsuario", $this->idTipoUsuario);
              $stmt->bindParam("estadoUsuario", $this->estadoUsuario);
            } 
          } else {
              return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }

    }*/

    public function updateUser(){

        try {
            $query = "UPDATE
                      " . $this->table_name . "
                  SET
                    u_name = :u_name,
                    u_lastname = :u_lastname,
                    u_username = :u_username,
                    u_password = :u_password,
                    u_status = :u_status,
                    u_user_type = :u_user_type
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("u_name", $this->u_name);
            $stmt->bindParam("u_lastname", $this->u_lastname);
            $stmt->bindParam("u_username", $this->u_username);
            $stmt->bindParam("u_password", $this->u_password);
            $stmt->bindParam("u_status", $this->u_status);
            $stmt->bindParam("u_user_type", $this->u_user_type);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }

    public function deleteUser() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }

    }

}
?>
