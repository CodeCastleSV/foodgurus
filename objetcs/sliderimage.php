<?php

class SliderImage
{
    private $conn;
    private $table_name = "fg_slider_image";

    /** Slider Image items **/
    public $ID;
    public $si_name;
    public $si_image;
    public $si_description;
    public $si_status;
    public $si_created_at;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function readAll() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " ORDER BY si_created_at DESC";


            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function readOne() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->si_name   = $row['si_name'];
            $this->si_image = $row['si_image'];
            $this->si_description     = $row['si_description'];
            $this->si_status = $row['si_status'];
            $this->si_created_at = $row['si_created_at'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createSliderImage(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (si_name, si_image, si_description, si_status, si_created_at)
                   VALUES (:si_name, :si_image, :si_description, :si_status, :si_created_at)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("si_name", $this->si_name);
          $stmt->bindParam("si_image", $this->si_image);
          $stmt->bindParam("si_description", $this->si_description);
          $stmt->bindParam("si_status", $this->si_status);
          $stmt->bindParam("si_created_at", $this->si_created_at);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function updateSliderImage(){

        try {
            $query = "UPDATE
                      " . $this->table_name . "
                  SET
                    si_name = :si_name,
                    si_image = :si_image,
                    si_description = :si_description,
                    si_status = :si_status
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("si_name", $this->si_name);
            $stmt->bindParam("si_image", $this->si_image);
            $stmt->bindParam("si_description", $this->si_description);
            $stmt->bindParam("si_status", $this->si_status);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteSliderImage() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }

    /** Slider Items **/

    public function readSliderImage() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name. " WHERE si_status=1";

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}
?>
