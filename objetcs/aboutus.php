<?php

class AboutUs
{
    private $conn;
    private $table_name = "fg_about_us";

    /** Us Section items **/
    public $ID;
    public $au_section;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function readAll() {

        try {
            $query = "SELECT * FROM " . $this->table_name;


            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function readOne() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->au_section   = $row['au_section'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createAboutUs(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (au_section)
                   VALUES (:au_section)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("au_section", $this->au_section);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function updateAboutUs(){

        try {
            $query = "UPDATE " . $this->table_name . "
                  SET
                    au_section = :au_section
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("au_section", $this->au_section);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteAboutUs() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }

    /** Us Items **/

    public function readAboutUs() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name;

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}
?>
