<?php

class CustomerProject
{
    private $conn;
    private $table_name = "fg_customer_project";

    /** Customer Project items **/
    public $ID;
    public $cp_name;
    public $cp_image;
    public $cp_status;
    public $cp_created_at;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function readAll() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " ORDER BY cp_created_at DESC";


            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function readOne() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->cp_name   = $row['cp_name'];
            $this->cp_image = $row['cp_image'];
            $this->cp_status     = $row['cp_status'];
            $this->cp_created_at = $row['cp_created_at'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createCustomerProject(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (cp_name, cp_image, cp_status, cp_created_at)
                   VALUES (:cp_name, :cp_image, :cp_status, :cp_created_at)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("cp_name", $this->cp_name);
          $stmt->bindParam("cp_image", $this->cp_image);
          $stmt->bindParam("cp_status", $this->cp_status);
          $stmt->bindParam("cp_created_at", $this->cp_created_at);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function updateCustomerProject(){

        try {
            $query = "UPDATE
                      " . $this->table_name . "
                  SET
                    cp_name = :cp_name,
                    cp_image = :cp_image,
                    cp_status = :cp_status
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("cp_name", $this->cp_name);
            $stmt->bindParam("cp_image", $this->cp_image);
            $stmt->bindParam("cp_status", $this->cp_status);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteCustomerProject() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }

    /** Customer Project Items **/

    public function readCustomerProject() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name. " WHERE cp_status=1";

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}
?>
