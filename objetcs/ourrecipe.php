<?php

class OurRecipe
{
    private $conn;
    private $table_name = "fg_our_recipe";

    /** Our Recipe items **/
    public $ID;
    public $or_name;
    public $or_image;
    public $or_description;
    public $or_status;
    public $or_created_at;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function readAll() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " ORDER BY or_created_at DESC";


            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function readOne() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->or_name   = $row['or_name'];
            $this->or_image = $row['or_image'];
            $this->or_description     = $row['or_description'];
            $this->or_status     = $row['or_status'];
            $this->or_created_at = $row['or_created_at'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createOurRecipe(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (or_name, or_image, or_description, or_status, or_created_at)
                   VALUES (:or_name, :or_image, :or_description, :or_status, :or_created_at)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("or_name", $this->or_name);
          $stmt->bindParam("or_image", $this->or_image);
          $stmt->bindParam("or_description", $this->or_description);
          $stmt->bindParam("or_status", $this->or_status);
          $stmt->bindParam("or_created_at", $this->or_created_at);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function updateOurRecipe(){

        try {
            $query = "UPDATE
                      " . $this->table_name . "
                  SET
                    or_name = :or_name,
                    or_image = :or_image,
                    or_description = :or_description,
                    or_status = :or_status
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("or_name", $this->or_name);
            $stmt->bindParam("or_image", $this->or_image);
            $stmt->bindParam("or_description", $this->or_description);
            $stmt->bindParam("or_status", $this->or_status);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteOurRecipe() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }

    /** Our Recipe Items **/

    public function readOurRecipe() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name. " WHERE or_status=1";

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}
?>
