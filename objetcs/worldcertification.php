<?php

class WorldCertification
{
    private $conn;
    private $table_name = "fg_world_certification";

    /** World Certification Section items **/
    public $ID;
    public $wc_section;

    function __construct($db)
    {
        $this->conn = $db;
    }

    public function readAll() {

        try {
            $query = "SELECT * FROM " . $this->table_name;


            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function readOne() {

        try {
            $query = "SELECT * FROM " . $this->table_name . " WHERE ID=:ID";

            $stmt = $this->conn->prepare( $query );
            $stmt->bindParam("ID", $this->ID);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->wc_section   = $row['wc_section'];
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createWorldCertification(){

        try {
          $query = "INSERT INTO " . $this->table_name . "
                   (wc_section)
                   VALUES (:wc_section)";

          $stmt = $this->conn->prepare( $query );

          $stmt->bindParam("wc_section", $this->wc_section);

          if($stmt->execute()) {
            return  true;
          } else {
            return false;
          }
        } catch (PDOException $e) {
          echo $e->getMessage();
        }
    }

    public function updateWorldCertification(){

        try {
            $query = "UPDATE " . $this->table_name . "
                  SET
                    wc_section = :wc_section
                  WHERE
                    ID = :ID";

            $stmt = $this->conn->prepare( $query );

            $stmt->bindParam("ID", $this->ID);
            $stmt->bindParam("wc_section", $this->wc_section);

            if($stmt->execute()) {
                return  true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteWorldCertification() {

        try {
            $query = "DELETE FROM ". $this->table_name ." WHERE ID = :ID";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam("ID", $this->ID);

            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $ex){
            echo $ex->getMessage();
        }
    }

    /** World Certification Items **/

    public function readWorldCertification() {

        try {
            $query = "SELECT *
            FROM " . $this->table_name;

            $stmt = $this->conn->prepare( $query );
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

    }
}
?>
