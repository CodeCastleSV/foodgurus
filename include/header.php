<!-- Menú Navbar Móvil- Begin -->
<header class="d-block d-sm-none">
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="index.php"><img src="images/foodgurus.png" class="img-fluid" alt="Food Gurus" style="width: 200px; height: auto;" ></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#nosotros">Nosotros</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#certificacionInternacional">Certificación Mundial</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#nuestraReceta">Nuestra Receta (Servicios)</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#alianzaInternacional">Alianza Internacional</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#contactanos">Contacto</a>
        </li>
      </ul>
      <div class="form-inline mt-2 mt-md-0">
        <!--<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
      </div>
    </div>
  </nav>
</header>
<!-- Menú Navbar Móvil - Begin -->

<!-- Menú Navbar Desktop - Begin -->
<header class="container-fluid blog-header py-4 d-none d-sm-block d-sm-none d-md-block">
  <div class="row flex-nowrap justify-content-between align-items-center">
    <div class="col-5 pt-0 text-right">
      <a class="text-muted" href="index.php">Inicio</a>
      <a class="text-muted" href="#nosotros">Nosotros</a>
      <a class="text-muted" href="#certificacionInternacional">Certificación<br>Mundial</a>
      <a class="text-muted" href="#nuestraReceta">Nuestra Receta<br><small>(Servicios)</small></a>
    </div>
    <div class="col-2 text-center">
      <a class="blog-header-logo text-dark" href="#"><img src="images/foodgurus.png" class="img-fluid" alt="Food Gurus"></a>
    </div>
    <div class="col-5 pt-0 text-left">
      <a class="text-muted" href="#alianzaInternacional">Alianza<br>Internacional</a>
      <a class="text-muted" href="#proyectos">Proyectos</a>
      <a class="text-muted" href="#testimoniales">Testimoniales</a>
      <a class="text-muted" href="#contactanos">Contáctanos</a>
    </div>
  </div>
</header>
<!-- Manú Navbar Desktop - End -->