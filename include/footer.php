<footer>
	<div class="row margin-padding-none">
		<div class="col-10 offset-1 col-md-2 offset-md-1">
			<img src="images/foodgurus.png" class="img-fluid" alt="Food Guru">
			<br><br>
		</div>
		<div class="col-10 offset-1 col-md-8 offset-md-0">
			<div class="row">
				<div class="col-md-3">
					<h5>EMAIL:</h5>
					<p><a href="mailto:juan@food-gurus.com">juan@food-gurus.com</a></p>
				</div>
				<div class="col-md-3">
					<h5>TELÉFONO:</h5>
					<p>(503) 2207-4523<br>(503) 7910-3154</p>
				</div>
				<div class="col-md-6">
					<h5>DIRECCIÓN:</h5>
					<p>Paseo General Escalón y Calle Arturo Ambrogi Bis#137<br>Colonia Escalón, San Salvador, El Salvador, Centroamérica</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row margin-padding-none">
		<div class="col-xs-12 col-md-4 offset-md-4 text-center">
			<a href="https://www.facebook.com/foodguruscentroamerica" target="_blank" title="Visitar Fan Page de Facebook">
				<img src="images/items/fb.png" class="img-fluid hvr-pulse" alt="Facebook" style="margin: auto; width: 60px; padding: 5px;">
			</a>
			<a href="http://www.instagram.com" target="_blank" title="Visitar Food Gurus en Instagram">
				<img src="images/items/in.png" class="img-fluid hvr-pulse" alt="Instagram" style="margin: auto; width: 60px; padding: 5px;">
			</a>
			<a href="https://www.youtube.com/watch?v=C9AsNlv_U9Q" target="_blank" title="Visitar Canal de Youtube">
				<img src="images/items/yt.png" class="img-fluid hvr-pulse" alt="Youtube" style="margin: auto; width: 60px; padding: 5px;">
			</a>
		</div>
	</div>
	<div class="row margin-padding-none">
		<div class="col-xs-12 col-md-12">
			<br>
			<p class="text-center">Food gurus agencia gastronómica, El Salvador 2018 - Todos los Derechos Reservados<br>Sitio diseñado y desarrollado por <a href="http://www.codecastle.com.sv">Code Castle El Salvador</a></p>
		</div>
	</div>
</footer>